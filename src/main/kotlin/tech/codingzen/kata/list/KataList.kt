package tech.codingzen.kata.list

import tech.codingzen.kata.list.KataList.Companion.katalist

sealed class KataList<out T> : AbstractCollection<T>() {
  object Nil : KataList<Nothing>()
  class Cons<out T>(override val head: T, override val tail: KataList<T>) : KataList<T>() {
    override fun equals(other: Any?): Boolean {
      if (other === this) return true
      if (other !is KataList<*>) return false

      val otherIterator = other.iterator()
      if (!otherIterator.hasNext()) return false
      if (head != otherIterator.next()) return false
      for (t in tail) {
        if (!otherIterator.hasNext()) return false
        if (t != otherIterator.next()) return false
      }

      return !otherIterator.hasNext()
    }
  }

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                CREATION
   * -------------------------- ---------------------------------------------------------------------------------------- */
  companion object {
    fun <T> nil(): KataList<T> = Nil
    fun <T> cons(head: T, tail: KataList<T> = nil()): KataList<T> = Cons(head, tail)

    fun <T> katalist(vararg elements: T): KataList<T> =
      elements.foldRight(nil()) { t, list -> cons(t, list) }
  }

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                PUT
   * ------------------------------------------------------------------------------------------------------------------ */
  fun cons(element: @UnsafeVariance T): KataList<T> = Cons(element, this)
  operator fun plus(element: @UnsafeVariance T): KataList<T> = Cons(element, this)
  fun consAll(elements: Collection<@UnsafeVariance T>): KataList<T> =
    elements.fold(this) { acc, t -> acc + t }

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                QUERY
   * ------------------------------------------------------------------------------------------------------------------ */
  override val size: Int get() = fold(0) { acc, _ -> acc + 1 }
  override fun isEmpty(): Boolean = isNil

  open val head: T
    get() = when (this) {
      is Nil -> throw NoSuchElementException("Cannot access head on empty list")
      is Cons -> head
    }

  open val tail: KataList<T>
    get() = when (this) {
      is Nil -> nil()
      is Cons -> tail
    }

  val isNil: Boolean
    get() = when (this) {
      is KataList.Nil -> true
      else -> false
    }

  val isCons: Boolean
    get() = when (this) {
      is KataList.Cons -> true
      else -> false
    }

  val asCons: Cons<T>
    get() = when (this) {
      is Cons -> this
      else -> throw IllegalStateException("KataList is Nil not Cons")
    }

  val asNil: Nil
    get() = when (this) {
      is Nil -> this
      else -> throw IllegalStateException("KataList is Cons not Nil")
    }

  fun indexOf(element: @UnsafeVariance T): Int = indexOfFirst(element)
  fun indexOfFirst(element: @UnsafeVariance T): Int = (this as Iterable<T>).indexOfFirst { it == element }
  fun indexOfLast(element: @UnsafeVariance T): Int = (this as Iterable<T>).indexOfLast { it == element }

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                GET
   * ------------------------------------------------------------------------------------------------------------------ */
  operator fun invoke(index: Int): T = get(index)
  operator fun get(index: Int): T {
    var j = 0
    for ((i, t) in withIndex()) {
      if (i == index) return t
      j = i
    }

    throw IndexOutOfBoundsException("index: $index is out of bounds for KataList of size: $j")
  }

  fun subList(fromIndex: Int, toIndex: Int): KataList<T> {
    val range = fromIndex until toIndex
    var sub: KataList<T> = nil()
    for ((i, t) in withIndex())
      when {
        i in range -> sub += t
        i > range.last -> break
      }
    return sub.reversed()
  }

  operator fun component1(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> head
  }

  operator fun component2(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> child0.head
    }
  }

  operator fun component3(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> child1.head
      }
    }
  }

  operator fun component4(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> when (val child2 = child1.tail) {
          is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 3 at index 3")
          is Cons -> child2.head
        }
      }
    }
  }

  operator fun component5(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> when (val child2 = child1.tail) {
          is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 3 at index 3")
          is Cons -> when (val child3 = child2.tail) {
            is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 4 at index 4")
            is Cons -> child3.head
          }
        }
      }
    }
  }

  operator fun component6(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> when (val child2 = child1.tail) {
          is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 3 at index 3")
          is Cons -> when (val child3 = child2.tail) {
            is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 4 at index 4")
            is Cons -> when (val child4 = child3.tail) {
              is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 5 at index 5")
              is Cons -> child4.head
            }
          }
        }
      }
    }
  }

  operator fun component7(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> when (val child2 = child1.tail) {
          is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 3 at index 3")
          is Cons -> when (val child3 = child2.tail) {
            is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 4 at index 4")
            is Cons -> when (val child4 = child3.tail) {
              is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 5 at index 5")
              is Cons -> when (val child5 = child4.tail) {
                is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 6 at index 6")
                is Cons -> child5.head
              }
            }
          }
        }
      }
    }
  }

  operator fun component8(): T = when (this) {
    is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 0 at index 0")
    is Cons -> when (val child0 = tail) {
      is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 1 at index 1")
      is Cons -> when (val child1 = child0.tail) {
        is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 2 at index 2")
        is Cons -> when (val child2 = child1.tail) {
          is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 3 at index 3")
          is Cons -> when (val child3 = child2.tail) {
            is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 4 at index 4")
            is Cons -> when (val child4 = child3.tail) {
              is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 5 at index 5")
              is Cons -> when (val child5 = child4.tail) {
                is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 6 at index 6")
                is Cons -> when (val child6 = child5.tail) {
                  is Nil -> throw IndexOutOfBoundsException("Attempting to destructure a KataList of size 7 at index 7")
                  is Cons -> child6.head
                }
              }
            }
          }
        }
      }
    }
  }

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                DEL
   * ------------------------------------------------------------------------------------------------------------------ */
  operator fun minus(element: @UnsafeVariance T): KataList<T> =
    fold(nil<T>()) { acc, t -> if (t == element) acc else acc + t }.reversed()

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                ITERATION
   * ------------------------------------------------------------------------------------------------------------------ */
  override fun iterator(): Iterator<T> = KataListIterator(this)

  fun reversed(): KataList<T> = fold(nil()) { acc, t -> acc + t }

  inline fun <U> map(f: (T) -> U): KataList<U> =
    fold<T, KataList<U>>(nil()) { acc, t -> acc + f(t) }.reversed()

  inline fun <U> flatMap(f: (T) -> KataList<U>): KataList<U> =
    fold<T, KataList<U>>(nil()) { acc, t ->
      f(t).fold(acc) { acc2, u -> acc2 + u}
    }.reversed()

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                REPR
   * ------------------------------------------------------------------------------------------------------------------ */
  override fun toString(): String = joinToString(",", "(", ")")

  /* ------------------------------------------------------------------------------------------------------------------
   *                                                EQUALITY
   * ------------------------------------------------------------------------------------------------------------------ */
  override fun equals(other: Any?): Boolean {
    if (other === this) return true
    if (other !is KataList<*>) return false

    val otherIterator = other.iterator()
    for (t in this) {
      if (!otherIterator.hasNext()) return false
      if (t != otherIterator.next()) return false
    }

    return !otherIterator.hasNext()
  }

  override fun hashCode(): Int {
    var hashCode = 1
    for (t in this) {
      hashCode = 31 * hashCode + (t?.hashCode() ?: 0)
    }
    return hashCode
  }
}

class KataListIterator<out T>(private var source: KataList<T>) : Iterator<T> {
  override fun hasNext(): Boolean = source.isCons

  override fun next(): T {
    if (!hasNext()) throw NoSuchElementException("No more elements from KataList")
    source.asCons.run {
      source = tail
      return head
    }
  }
}

fun main(args: Array<String>) {
  val sqft = listOf(18.6, 3.1, 1.5, 1.5, 4.0, 1.0, 20.0, 11.5, 16.0, 11.5, 19.0, 10.0).sum() * 10.25
  katalist("a").let { (a) -> println("$a") }
  katalist("a", "b").let { (a, b) -> println("$a,$b") }
  katalist("a", "b", "c").let { (a, b, c) -> println("$a,$b,$c") }
  katalist("a", "b", "c", "d").let { (a, b, c, d) -> println("$a,$b,$c,$d") }
  katalist("a", "b", "c", "d", "e").let { (a, b, c, d, e) -> println("$a,$b,$c,$d,$e") }
  katalist("a", "b", "c", "d", "e", "f").let { (a, b, c, d, e, f) -> println("$a,$b,$c,$d,$e,$f") }
  katalist("a", "b", "c", "d", "e", "f", "g").let { (a, b, c, d, e, f, g) -> println("$a,$b,$c,$d,$e,$f,$g") }
  katalist(
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h"
  ).let { (a, b, c, d, e, f, g, h) -> println("$a,$b,$c,$d,$e,$f,$g,$h") }

  println(sqft)

  val x = KataList.nil<Int>() + 2 + 3
}