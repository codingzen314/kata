package tech.codingzen.kata.tuple

data class KTuple2<out P0, out P1>(
  val part0: P0,
  val part1: P1
)

fun <P0, P1, Q0> KTuple2<P0, P1>.set0(q0: Q0): KTuple2<Q0, P1> = KTuple2(q0, part1)

fun <P0, P1, Q1> KTuple2<P0, P1>.set1(q1: Q1): KTuple2<P0, Q1> = KTuple2(part0, q1)

inline fun <P0, P1, Q0> KTuple2<P0, P1>.map0(fn: (P0) -> Q0): KTuple2<Q0, P1> =
  KTuple2(fn(part0), part1)

inline fun <P0, P1, Q1> KTuple2<P0, P1>.map1(fn: (P1) -> Q1): KTuple2<P0, Q1> =
  KTuple2(part0, fn(part1))
