package tech.codingzen.kata.tuple

data class KTuple1<out P0>(
  val part0: P0
)

fun <P0, Q0> KTuple1<P0>.set0(q0: Q0): KTuple1<Q0> = KTuple1(q0)

inline fun <P0, Q0> KTuple1<P0>.map0(fn: (P0) -> Q0): KTuple1<Q0> = KTuple1(fn(part0))
