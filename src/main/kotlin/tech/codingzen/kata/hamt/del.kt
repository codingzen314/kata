package tech.codingzen.kata.hamt

import tech.codingzen.kata.hamt.HamtNode.Companion.bitpos
import tech.codingzen.kata.hamt.HamtNode.Companion.idx

fun HamtNode.del(hash: Int, shift: Int, key: Any): HamtNode? = when (this) {
    is HashCollisionNode -> del(hash, shift, key)
    is ArrayNode -> del(hash, shift, key)
    is PartialNode -> del(hash, shift, key)
}

fun HashCollisionNode.del(hash: Int, shift: Int, key: Any): HamtNode? {
    val idx = (0 until count)
        .find { key == storage[2 * it] }
    return when {
        idx == null -> this
        count <= 1 -> null
        else -> HashCollisionNode(this.hash, count - 1, storage.removePair(idx!!))
    }
}

fun ArrayNode.del(hash: Int, shift: Int, key: Any): HamtNode? {
    val idx = idx(hash, shift)
    return when (val child = nodes[idx]) {
        null -> this
        else -> when (val newChild = child.del(hash, shift + 5, key)) {
            null ->
                if (count == 1) null
                else ArrayNode(count - 1, nodes.clone().apply {
                    this[idx] = null
                })
            else ->
                if (child == newChild) this
                else ArrayNode(count, nodes.clone().apply {
                    this[idx] = newChild
                })
        }
    }
}

fun PartialNode.del(hash: Int, shift: Int, key: Any): HamtNode? {
    val bitp = bitpos(hash, shift)
    return if (bitmap and bitp == 0) this
    else {
        val idx = Integer.bitCount(bitmap and (bitp - 1))
        val (k, v) = storage[2 * idx] to storage[2 * idx + 1]
        when (k) {
            null ->
                (v as HamtNode).del(hash, shift + 5, key)
                    ?.let { newChild ->
                        if (v == newChild) this
                        else PartialNode(bitmap, storage.clone().apply {
                            this[2 * idx + 1] = newChild
                        })
                    }
                    ?: run {
                        if (bitmap == bitp) null
                        else PartialNode(bitmap xor bitp, storage.removePair(idx))
                    }
            key ->
                if (bitmap == bitp) null
                else PartialNode(bitmap xor bitp, storage.removePair(idx))
            else -> this
        }
    }
}

inline fun <reified A> Array<A?>.removePair(idx: Int): Array<A?> = arrayOfNulls<A>(this.size - 2).apply {
    System.arraycopy(this@removePair, 0, this, 0, 2 * idx)
    System.arraycopy(this@removePair, 2 * idx + 2, this, 2 * idx, this.size - 2 * idx)
}