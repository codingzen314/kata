package tech.codingzen.kata.hamt

sealed class HamtNode : Iterable<InternalEntry> {
    companion object {
        val empty: HamtNode = PartialNode(0, arrayOf())
        fun single(hash: Int, shift: Int, key: Any, value: Any?): HamtNode =
            PartialNode(bitpos(hash, shift), arrayOf(key, value))

        fun idx(hash: Int, shift: Int): Int = hash.ushr(shift) and 0x01F
        fun bitpos(hash: Int, shift: Int): Int = 1 shl idx(hash, shift)
    }
}

/**
 * Only stores references to other [HamtNode]
 *
 * ### Count Invariant
 * - [count]: `9 <= count <= 32`  If any ArrayNode would store less than 9 elements then that node must be converted
 *   to a [PartialNode]
 *
 * @param count the number of references held
 * @param nodes holds the references
 */
data class ArrayNode(val count: Int, val nodes: Array<HamtNode?>) : HamtNode() {
    override fun iterator(): Iterator<InternalEntry> = ArrayNodeIterator()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArrayNode

        if (count != other.count) return false
        if (!nodes.contentEquals(other.nodes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = count
        result = 31 * result + nodes.contentHashCode()
        return result
    }

    inner class ArrayNodeIterator : Iterator<InternalEntry> {
        var index = 0
        var iterator: Iterator<InternalEntry>? = null

        override fun hasNext(): Boolean =
            when {
                iterator == null -> findNext()
                iterator!!.hasNext() -> true
                else -> findNext()
            }

        private fun findNext(): Boolean {
            for (i in index until 32) {
                val iter = nodes[i]?.iterator()?.takeIf { it.hasNext() } ?: continue
                iterator = iter
                index = i + 1
                return true
            }
            return false
        }

        override fun next(): InternalEntry =
            if (hasNext()) iterator!!.next()
            else throw NoSuchElementException()
    }
}

/**
 * Stores a hash and Key-Values that have that same hash
 *
 * ### Storage Invariants
 * - Keys are stored in even indices
 * - Values are stored in odd indices
 * - Storage size may be more than [count]  this facilitates quicker removal by just changing [count]
 *
 * ### Hash Invariant
 * - All keys must be equal to [hash] however those keys may be different with respect to [Any.equals]
 *
 *
 * @param hash the hash that is common to all values held in this node
 * @param count the number of key-values stored
 * @param storage holds the Key-Values
 */
data class HashCollisionNode(val hash: Int, val count: Int, val storage: Array<Any?>) : HamtNode() {
    override fun iterator(): Iterator<InternalEntry> = HashCollisionNodeIterator()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HashCollisionNode

        if (hash != other.hash) return false
        if (count != other.count) return false
        if (!storage.contentEquals(other.storage)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = hash
        result = 31 * result + count
        result = 31 * result + storage.contentHashCode()
        return result
    }

    inner class HashCollisionNodeIterator : Iterator<InternalEntry> {
        var current: InternalEntry? = null
        var idx = 0

        override fun hasNext(): Boolean =
            when {
                count == 0 -> false
                current != null -> true
                idx >= count * 2 -> false
                else -> {
                    current = InternalEntry(storage[idx]!!, storage[idx + 1])
                    idx += 2
                    true
                }
            }

        override fun next(): InternalEntry =
            if (hasNext()) {
                val result = current!!
                current = null
                result
            } else throw NoSuchElementException()
    }
}

/**
 * Stores both references and values
 *
 * ### Storage Invariants
 * - Keys are stored in even indices
 * - Values are stored in odd indices
 * - Key-Values/References are stored in pairs located at indices `2*i` and `2*i+1` where `0 <= i <= 8`
 * - Keys are stored at index `2i` and the corresponding values at index `2i + 1`
 * - References to another [HamtNode] are stored at index `2i + 1` and `null` must be stored at index `2i`
 * - [storage] must have size equal to `2*count`
 *
 * ### Count Invariants
 * - `0 <= count <= 16`  If any BitmapIndexedNode would store more than 16 elements then that node must be
 *   converted to an [ArrayNode]
 * - count must be the number of 1 bits in [bitmap]
 *
 * @param bitmap stores all the indices of Key-Value/References and the number of Key-Value/References
 * @param storage stores the Key-Value and/or References
 */
data class PartialNode(val bitmap: Int, val storage: Array<Any?>) : HamtNode() {
    override fun iterator(): Iterator<InternalEntry> = PartialNodeIterator()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PartialNode

        if (bitmap != other.bitmap) return false
        if (!storage.contentEquals(other.storage)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = bitmap
        result = 31 * result + storage.contentHashCode()
        return result
    }

    inner class PartialNodeIterator : Iterator<InternalEntry> {
        var index = 0
        var current: InternalEntry? = null
        var iterator: Iterator<InternalEntry>? = null

        override fun hasNext(): Boolean =
            when {
                storage.isEmpty() -> false
                current != null -> true
                iterator == null -> findNext()
                iterator!!.hasNext() -> true
                else -> findNext()
            }

        private fun findNext(): Boolean {
            for (i in index until storage.size step 2) {
                val (key, value) = storage[index] to storage[index + 1]
                if (key == null) {
                    iterator = (value as HamtNode).iterator().takeIf { it.hasNext() } ?: continue
                } else {
                    current = InternalEntry(key, value)
                }
                index = i + 2
                return true
            }
            return false
        }

        override fun next(): InternalEntry =
            if (hasNext())
                iterator?.takeIf { it.hasNext() }?.next() ?: run {
                    val result = current!!
                    current = null
                    result
                }
            else throw NoSuchElementException()
    }
}

data class InternalEntry(override val key: Any, override val value: Any?) : Map.Entry<Any, Any?>