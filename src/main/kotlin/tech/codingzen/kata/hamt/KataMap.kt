package tech.codingzen.kata.hamt

import tech.codingzen.kata.tuple.*

/**
 * Persistent/Immutable implementation of a HashMap.  This is a port of [Rich Hickey's Clojure PersistentHashmap](https://github.com/clojure/clojure/blob/master/src/jvm/clojure/lang/PersistentHashMap.java)
 * __Create__
 * ```
 * val empty = KataMap()
 * val map = KataMap("a" to 1, "b" to 2)
 * ```
 *
 * __Put__
 * ```
 * KataMap() + ("a" to 1")
 * KataMap().put("a", 1)
 * ```
 *
 * __Del__
 * ```
 * val map = KataMap("a" to 1, "b" to 2)
 * val onlyB = map - "a"
 * val empty = map.del("a") - "b"
 * ```
 *
 * __Get__
 * ```
 * val map = KataMap("a" to 1, "b" to 2)
 * val aValue = map["a"]
 * val none = map["c"] // null
 * val (a,b,c) = map["a", "b", "c"] // destructuring up to arity 8 (inclusive)
 * ```
 *
 * __Size__
 * ```
 * val map = KataMap("a" to 1, "b" to 2)
 * map.size
 * map.isEmpty()
 * map.isNotEmpty()
 * ```
 *
 * __Query__
 * ```
 * val map = KataMap("a" to 1, "b" to 2)
 * map.containsKey("a")
 * map.containsValue(2)
 * "a" in map
 * "c" !in map
 * ```
 *
 * __Iteration__
 * ```
 * val map = KataMap("a" to 1, "b" to 2)
 * for ((k, v) in map) {
 *   //...
 * }
 * map.iterator()
 * map.asSequence() // Sequence<Map.Entry<String, Int>>
 * map.entries // Set<Map.Entry<String, Int>>
 * map.keys // Set<String>
 * map.values // Collection<Int>
 * ```
 *
 * @param K key type
 * @param V value type
 */
class KataMap<K, out V> private constructor(
    override val size: Int,
    private val root: HamtNode
) : Map<K, V> {
    /* ------------------------------------------------------------------------------------------------------------------
     *                                                CREATION
     * -------------------------- ---------------------------------------------------------------------------------------- */
    companion object {
        /**
         * @return an empty [KataMap]
         * @param K key type
         * @param V value type
         */
        fun <K, V> empty(): KataMap<K, V> = KataMap(0, HamtNode.empty)

        /**
         * @return a [KataMap] containing all the provided key-value pairs [kvs]
         * @param kvs key-value pairs used to build the map
         * @param K key type
         * @param V value type
         */
        operator fun <K, V> invoke(vararg kvs: Pair<K, V>): KataMap<K, V> =
            kvs.fold(empty()) { kmap, (k, v) ->
                kmap.put(k, v)
            }
    }

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                PUT
     * ------------------------------------------------------------------------------------------------------------------ */

    /**
     * @return a new [KataMap] containing all the key-value pairs of this KataMap and key-value pair [k], [v]
     * Note that [k],[v] will overwrite corresponding key-value pairs in this KataMap
     * @param k key
     * @param v value
     */
    fun put(k: @UnsafeVariance K, v: @UnsafeVariance V): KataMap<K, V> {
        val addedLeaf = AddedLeaf()
        val newRoot = root.put(addedLeaf, k.hashCode(), 0, k as Any, v)
        return KataMap(if (addedLeaf.added) size + 1 else size, newRoot)
    }

    /**
     * @return a new [KataMap] containing all key-value pairs of this KataMap and key-value pairs [kv] and [kvs]
     * @param kv key-value pair to add
     * @param kvs key-value pairs to add
     */
    fun put(kv: Pair<K, @UnsafeVariance V>, vararg kvs: Pair<K, @UnsafeVariance V>): KataMap<K, V> =
        kvs.fold(put(kv.first, kv.second)) { acc, (k, v) -> acc.put(k, v) }

    /**
     * @return a new [KataMap] containing all key-value pairs of this KataMap and key-value pairs [kv] and [kvs]
     * @param kv key-value pair to add
     * @param kvs key-value pairs to add
     */
    fun put(kv: Map.Entry<K, @UnsafeVariance V>, vararg kvs: Map.Entry<K, @UnsafeVariance V>): KataMap<K, V> =
        kvs.fold(put(kv.key, kv.value)) { acc, (k, v) -> acc.put(k, v) }

    /**
     * @return a new [KataMap] containing all key-value pairs of this KataMap and [kv]
     * Note that [kv] will overwrite corresponding key-value pairs in this KataMap
     * @param kv key-value pair to add
     */
    operator fun plus(kv: Pair<K, @UnsafeVariance V>): KataMap<K, V> = put(kv.first, kv.second)

    /**
     * @return a new [KataMap] containing all key-value pairs of this KataMap and [kv]
     * Note that [kv] will overwrite corresponding key-value pairs in this KataMap
     * @param kv key-value pair to add
     */
    operator fun plus(kv: Map.Entry<K, @UnsafeVariance V>): KataMap<K, V> = put(kv.key, kv.value)

    /**
     * @return a new [KataMap] containing all the key-value pairs of this KataMap and all key-values pairs of [other].
     * Note that key-value pairs in [other] will overwrite corresponding key-value pairs in this KataMap
     * @param other map to merge with this KataMap
     */
    infix operator fun plus(other: Map<K, @UnsafeVariance V>): KataMap<K, V> = merge(other)

    /**
     * @return a new [KataMap] containing all the key-value pairs of this KataMap and all key-values pairs of [other].
     * Note that key-value pairs in [other] will overwrite corresponding key-value pairs in this KataMap
     * @param other map to merge with this KataMap
     */
    fun merge(other: Map<K, @UnsafeVariance V>): KataMap<K, V> =
        other.entries.fold(this) { acc, (k, v) -> acc.put(k, v) }

    /**
     * @return a new [KataMap] containing all the key-value pairs of this KataMap and all key-values pairs of [kvs].
     * Note that key-value pairs in [kvs] will overwrite corresponding key-value pairs in this KataMap
     * @param kvs Collection of key-value pairs to merge with this Katamap
     */
    infix operator fun plus(kvs: Collection<Pair<K, @UnsafeVariance V>>): KataMap<K, V> =
        kvs.fold(this) { acc, (k, v) -> acc.put(k, v) }

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                DEL
     * ------------------------------------------------------------------------------------------------------------------ */

    /**
     * @return a new [KataMap] containing all key-value pairs except for [k]
     * @param k key to remove
     */
    fun del(k: @UnsafeVariance K): KataMap<K, V> =
        when (val newRoot = root.del(k.hashCode(), 0, k as Any)) {
            null -> empty()
            root -> this
            else -> KataMap(size - 1, newRoot)
        }

    /**
     * @return a new [KataMap] containing all key-value pairs except for [k] and [ks]
     * @param k key to remove
     * @param ks keys to remove
     */
    fun del(k: K, vararg ks: K): KataMap<K, V> = ks.fold(this - k) { acc, k -> acc - k }

    /**
     * @return a new [KataMap] containing all key-value pairs except for [k]
     * @param k key to remove
     */
    infix operator fun minus(k: K): KataMap<K, V> = del(k)

    /**
     * @return a new [KataMap] containing all key-value pairs except for [keys]
     * @param keys keys to remove
     */
    infix operator fun minus(keys: Collection<K>): KataMap<K, V> = keys.fold(this) { acc, k -> acc - k }

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                GET
     * ------------------------------------------------------------------------------------------------------------------ */

    /**
     * @return value or [null] associated with key [k]
     * @param k key to lookup
     */
    override operator fun get(k: @UnsafeVariance K): V? = root.get(k.hashCode(), 0, k as Any)?.value as V

    /**
     * @return value or [null] associated with each key [k0], [k1]
     * @param k0 key to lookup
     * @param k1 key to lookup
     */
    operator fun get(k0: @UnsafeVariance K, k1: @UnsafeVariance K): Pair<V?, V?> = get(k0) to get(k1)

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     */
    operator fun get(k0: @UnsafeVariance K, k1: @UnsafeVariance K, k2: @UnsafeVariance K): Triple<V?, V?, V?> =
        Triple(get(k0), get(k1), get(k2))

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2], [k3]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     * @param k3 key to lookup
     */
    operator fun get(
        k0: @UnsafeVariance K,
        k1: @UnsafeVariance K,
        k2: @UnsafeVariance K,
        k3: @UnsafeVariance K
    ): KTuple4<V?, V?, V?, V?> =
        KTuple(get(k0), get(k1), get(k2), get(k3))

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2], [k3], [k4]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     * @param k3 key to lookup
     * @param k4 key to lookup
     */
    operator fun get(
        k0: @UnsafeVariance K,
        k1: @UnsafeVariance K,
        k2: @UnsafeVariance K,
        k3: @UnsafeVariance K,
        k4: @UnsafeVariance K
    ): KTuple5<V?, V?, V?, V?, V?> =
        KTuple(get(k0), get(k1), get(k2), get(k3), get(k4))

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2], [k3], [k4], [k5]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     * @param k3 key to lookup
     * @param k4 key to lookup
     * @param k5 key to lookup
     */
    operator fun get(
        k0: @UnsafeVariance K,
        k1: @UnsafeVariance K,
        k2: @UnsafeVariance K,
        k3: @UnsafeVariance K,
        k4: @UnsafeVariance K,
        k5: @UnsafeVariance K
    ): KTuple6<V?, V?, V?, V?, V?, V?> =
        KTuple(get(k0), get(k1), get(k2), get(k3), get(k4), get(k5))

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2], [k3], [k4], [k5], [k6]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     * @param k3 key to lookup
     * @param k4 key to lookup
     * @param k5 key to lookup
     * @param k6 key to lookup
     */
    operator fun get(
        k0: @UnsafeVariance K,
        k1: @UnsafeVariance K,
        k2: @UnsafeVariance K,
        k3: @UnsafeVariance K,
        k4: @UnsafeVariance K,
        k5: @UnsafeVariance K,
        k6: @UnsafeVariance K
    ): KTuple7<V?, V?, V?, V?, V?, V?, V?> =
        KTuple(get(k0), get(k1), get(k2), get(k3), get(k4), get(k5), get(k6))

    /**
     * @return value or [null] associated with each key [k0], [k1], [k2], [k3], [k4], [k5], [k6], [k7]
     * @param k0 key to lookup
     * @param k1 key to lookup
     * @param k2 key to lookup
     * @param k3 key to lookup
     * @param k4 key to lookup
     * @param k5 key to lookup
     * @param k6 key to lookup
     * @param k7 key to lookup
     */
    operator fun get(
        k0: @UnsafeVariance K,
        k1: @UnsafeVariance K,
        k2: @UnsafeVariance K,
        k3: @UnsafeVariance K,
        k4: @UnsafeVariance K,
        k5: @UnsafeVariance K,
        k6: @UnsafeVariance K,
        k7: @UnsafeVariance K
    ): KTuple8<V?, V?, V?, V?, V?, V?, V?, V?> =
        KTuple(get(k0), get(k1), get(k2), get(k3), get(k4), get(k5), get(k6), get(k7))

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                QUERY
     * ------------------------------------------------------------------------------------------------------------------ */
    /**
     * @return [true] if this KataMap contains [key], [false otherwise]
     * @param key key to lookup
     */
    operator fun contains(key: K): Boolean = contains(key)

    /**
     * @return [true] if this KataMap contains [key], [false otherwise]
     * @param key key to lookup
     */
    override fun containsKey(key: K): Boolean = get(key) != null

    /**
     * @return [true] if this KataMap contains [value], [false otherwise]
     * @param value value to lookup
     */
    override fun containsValue(value: @UnsafeVariance V): Boolean =
        asSequence().find { (_, v) -> v == value }?.let { true } ?: false

    /**
     * @return [true] if this map is empty, [false] otherwise
     */
    override fun isEmpty(): Boolean = size == 0

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                ITERATION
     * ------------------------------------------------------------------------------------------------------------------ */

    /**
     * A [Set] (backed by this KataMap) containing all key-value pairs in this KataMap
     */
    override val entries: Set<Map.Entry<K, V>>
        get() = object : AbstractSet<Map.Entry<K, V>>() {
            override val size: Int
                get() = this@KataMap.size

            override fun iterator(): Iterator<Map.Entry<K, V>> =
                this@KataMap.root.iterator() as Iterator<Map.Entry<K, V>>
        }

    /**
     * A [Set] (backed by this KataMap) containing all keys in this KataMap
     */
    override val keys: Set<K>
        get() = object : AbstractSet<K>() {
            override val size: Int
                get() = this@KataMap.size

            override fun iterator(): Iterator<K> =
                this@KataMap.root.iterator().asSequence().map { it.key }.iterator() as Iterator<K>
        }

    /**
     * A [Collection] containing all values in this KataMap
     */
    override val values: Collection<V>
        get() = ArrayList<V>(size).apply { this@KataMap.forEach { (_, v) -> add(v) } }

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                EQUALITY
     * ------------------------------------------------------------------------------------------------------------------ */

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as KataMap<K, V>

        if (size != other.size) return false
        if (root != other.root) return false

        return true
    }

    override fun hashCode(): Int {
        var result = size
        result = 31 * result + root.hashCode()
        return result
    }

    /* ------------------------------------------------------------------------------------------------------------------
     *                                                REPR
     * ------------------------------------------------------------------------------------------------------------------ */

    override fun toString(): String {
        return entries.joinToString(prefix = "{", separator = ",", postfix = "}") { (k, v) ->
            "($k,$v)"
        }
    }
}