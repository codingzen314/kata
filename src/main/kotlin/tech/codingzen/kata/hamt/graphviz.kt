package tech.codingzen.kata.hamt

//sealed class Child {
//  data class Ref(val node: Node) : Child()
//  data class KV(val k: Any?, val v: Any?) : Child()
//}
//
//data class Node(val base: HamtNode, val id: Int, val children: Map<Int, Child>)
//
//fun HamtNode.toNode(id: Int = 0): Pair<Int, Node> = when (this) {
//  is HashCollisionNode -> id + 1 to Node(this, id, emptyMap())
//  is ArrayNode -> {
//    var (nextId, children) = id + 1 to mutableMapOf<Int, Child>()
//    for ((idx, childNode) in nodes.withIndex()) {
//      if (childNode == null) continue
//      val (id, nd) = childNode.toNode(nextId)
//      children[idx] = Child.Ref(nd)
//      nextId = id
//    }
//    nextId to Node(this, id, children)
//  }
//  is PartialNode -> {
//    var (nextId, children) = id + 1 to mutableMapOf<Int, Child>()
//    for (i in (0 until storage.size / 2)) {
//      if (storage[2 * i] == null) {
//        val (id, nd) = (storage[2 * i + 1]!! as HamtNode).toNode(nextId)
//        children[i] = Child.Ref(nd)
//        nextId = id
//      } else {
//        children[i] = Child.KV(storage[2 * i], storage[2 * i + 1])
//      }
//    }
//    nextId to Node(this, id, children)
//  }
//}
//
//fun <R> Node.fold(initial: R, fold: (R, Node) -> R): R =
//  children.values.asSequence()
//    .filterIsInstance<Child.Ref>()
//    .map { it.node }
//    .fold(fold(initial, this)) { acc, node ->
//      node.fold(acc, fold)
//    }
//
//fun Node.toGraphviz(): String {
//  val nodes = fold(StringBuilder()) { sb, (base, id, children) ->
//    val s = when (base) {
//      is HashCollisionNode -> {
//        val parts = base.storage.apply { reverse() }.joinToString(" | ")
//        "n$id [label=\"{Hash | {$parts}}\"];"
//      }
//      is ArrayNode -> {
//        val parts = base.nodes.indices.reversed().map { i ->
//          if (base.nodes[i] == null) " "
//          else "<f$i> _"
//        }.joinToString(" | ")
//        "n$id [label=\"{Array | {$parts}}\"];"
//      }
//      is PartialNode -> {
//        val parts = (children.size - 1 downTo 0).map { i ->
//          when (val child = children[i]!!) {
//            is Child.KV -> "${child.v} | ${child.k}"
//            is Child.Ref -> "<f$i> _ | "
//          }
//        }.joinToString(" | ")
//        "n$id [label=\"{Partial | {$parts}}\"];"
//      }
//    }
//    sb.append(s).append(System.lineSeparator())
//  }
//
//  val edges = fold(StringBuilder()) { sb, (base, id, children) ->
//    val s = when (base) {
//      is HashCollisionNode -> null
//      is ArrayNode -> {
//        base.nodes.indices
//          .filter { base.nodes[it] != null }
//          .map { i ->
//            val childId = (children[i]!! as Child.Ref).node.id
//            "n$id:f$i -> n$childId;"
//          }.joinToString(System.lineSeparator())
//      }
//      is PartialNode -> {
//        (0 until base.storage.size / 2)
//          .map { i ->
//            when (val child = children[i]!!) {
//              is Child.KV -> null
//              is Child.Ref -> {
//                val childId = child.node.id
//                "n$id:f$i -> n$childId;"
//              }
//            }
//          }
//          .filterNotNull()
//          .joinToString(System.lineSeparator())
//      }
//    }
//    sb.append(s ?: "").append(System.lineSeparator())
//  }
//
//  return """digraph G {
//    node [shape=record];
//    $nodes
//    $edges
//  }""".trimIndent()
//}

//fun main() {
//    val alphabet = "abcdefghijklmnop"//qrstuvwxyz"
//    val keys = alphabet.mapIndexed { i, c -> c to tkey("k$c", p0 = i) }.toMap()
//    val vals = alphabet.map { it to "v$it" }.toMap()
//    val ka = tkey("kaa", p1 = 1, p0 = 1)
//    val va = "vaa"
//    val kb = tkey("kbb", p1 = 3, p0 = 2)
//    val vb = "vbb"
//    val kc = tkey("kcc", p0 = 2)
//    val vc = "vcc"
//    val kd = tkey("kdd", p0 = 7)
//    val vd = "vdd"
//    val s = alphabet.fold(HamtNode.empty) { node, c ->
//      val (k, v) = keys[c]!! to vals[c]!!
//      node.put(k.hash, 0, k, v)
//    }
//      .put(ka.hash, 0, ka,va)
//      .put(kb.hash, 0, kb,vb)
//      .put(kc.hash, 0, kc,vc)
//      .put(kd.hash, 0, kd,vd)
//      .toNode().second.toGraphviz()
//    println(s)
//}

