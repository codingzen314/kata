package tech.codingzen.kata.hamt

import tech.codingzen.kata.hamt.HamtNode.Companion.bitpos
import tech.codingzen.kata.hamt.HamtNode.Companion.idx

fun HamtNode.get(hash: Int, shift: Int, key: Any): InternalEntry? = when (this) {
    is PartialNode -> get(hash, shift, key)
    is ArrayNode -> get(hash, shift, key)
    is HashCollisionNode -> get(hash, shift, key)
}

fun ArrayNode.get(hash: Int, shift: Int, key: Any): InternalEntry? =
    if (nodes.isEmpty()) null
    else {
        val idx = idx(hash, shift)
        nodes[idx]?.get(hash, shift + 5, key)
    }

fun HashCollisionNode.get(hash: Int, shift: Int, key: Any): InternalEntry? =
    if (count == 0) null
    else (0 until (2 * count) step 2)
        .find { key == storage[it] }
        ?.let { InternalEntry(storage[it]!!, storage[it + 1]) }

fun PartialNode.get(hash: Int, shift: Int, key: Any): InternalEntry? {
    val bitp = bitpos(hash, shift)
    val idx = Integer.bitCount(bitmap and (bitp - 1))
    return if (bitmap and bitp == 0) null
    else storage[2 * idx]
        ?.let { InternalEntry(key, storage[2 * idx + 1]) }
        ?: (storage[2 * idx + 1] as HamtNode).get(hash, shift + 5, key)
}