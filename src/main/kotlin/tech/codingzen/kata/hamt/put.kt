package tech.codingzen.kata.hamt

import tech.codingzen.kata.hamt.HamtNode.Companion.bitpos
import tech.codingzen.kata.hamt.HamtNode.Companion.idx
import tech.codingzen.kata.hamt.HamtNode.Companion.single

data class AddedLeaf(var added: Boolean = false)

fun HamtNode.put(addedLeaf: AddedLeaf, hash: Int, shift: Int, key: Any, value: Any?): HamtNode = when (this) {
    is PartialNode -> put(addedLeaf, hash, shift, key, value)
    is ArrayNode -> put(addedLeaf, hash, shift, key, value)
    is HashCollisionNode -> put(addedLeaf, hash, shift, key, value)
}

fun HashCollisionNode.put(addedLeaf: AddedLeaf, hash: Int, shift: Int, key: Any, value: Any?): HamtNode =
    if (this.hash == hash) {
        val idx = (0 until (2 * count) step 2).find { key == storage[it] } ?: -1
        if (idx != -1) {
            if (storage[idx + 1] == value) this
            else copy(storage = storage.clone().apply {
                this[idx + 1] = value
            })
        } else copy(count = count + 1, storage = storage.copyOf(storage.size + 2).apply {
            this[2 * count] = key
            this[2 * count + 1] = value
            addedLeaf.added = true
        })
    } else {
        val idxNew = idx(hash, shift)
        val idxNode = idx(this.hash, shift)
        val storage = if (idxNew > idxNode) arrayOf(null, this, key, value) else arrayOf(key, value, null, this)
        addedLeaf.added = true
        PartialNode(bitpos(hash, shift) or bitpos(this.hash, shift), storage)
    }

fun ArrayNode.put(addedLeaf: AddedLeaf, hash: Int, shift: Int, key: Any, value: Any?): HamtNode {
    val idx = idx(hash, shift)
    val current = nodes[idx]
    return current
        ?.put(addedLeaf, hash, shift + 5, key, value)
        ?.let { updatedNode ->
            if (updatedNode == current) this
            else copy(nodes = nodes.clone().apply {
                this[idx] = updatedNode
            })
        }
        ?: ArrayNode(count + 1, nodes.clone().apply {
            addedLeaf.added = true
            this[idx] = single(hash, shift + 5, key, value)
        })
}

fun PartialNode.put(addedLeaf: AddedLeaf, hash: Int, shift: Int, key: Any, value: Any?): HamtNode {
    val bitp = bitpos(hash, shift)
    val idx = Integer.bitCount(bitmap and (bitp - 1))
    return if (bitmap and bitp == 0) { // no child at idx
        val count = Integer.bitCount(bitmap)
        addedLeaf.added = true
        if (count >= 16) { // to many children in this node so convert to ArrayNode
            val nodes = arrayOfNulls<HamtNode?>(32)
            nodes[idx(hash, shift)] = single(hash, shift + 5, key, value)
            var bitm = bitmap
            var j = 0
            for (i in 0 until 32) {
                if (bitm and 1 != 0) {
                    nodes[i] =
                        if (storage[j] == null) storage[j + 1] as HamtNode
                        else single(storage[j]!!.hashCode(), shift + 5, storage[j]!!, storage[j + 1])
                    j += 2
                }
                bitm = bitm ushr 1
            }
            ArrayNode(count + 1, nodes)
        } else { // add new child to PartialNode
            val storage = arrayOfNulls<Any?>(2 * (count + 1))
            System.arraycopy(this.storage, 0, storage, 0, 2 * idx)
            storage[2 * idx] = key
            storage[2 * idx + 1] = value
            System.arraycopy(this.storage, 2 * idx, storage, 2 * (idx + 1), 2 * (count - idx))
            PartialNode(bitmap or bitp, storage)
        }
    } else { // check for reference to child node or kv pair
        val (k, v) = storage[2 * idx] to storage[2 * idx + 1]
        if (k == null) { // reference to another node
            val updatedNode = (v as HamtNode).put(addedLeaf, hash, shift + 5, key, value)
            if (updatedNode == v) this
            else copy(storage = storage.clone().apply {
                this[2 * idx + 1] = updatedNode
            })
        } else if (k == key) { // update value
            if (v == value) this
            else copy(storage = storage.clone().apply {
                this[2 * idx + 1] = value
            })
        } else { // hashes match up to shift but k != key so create a node with both k,v and key,value
            val khash = k.hashCode()
            val node =
                if (hash == khash) HashCollisionNode(hash, 2, arrayOf(k, v, key, value)).apply {
                    addedLeaf.added = true
                }
                else single(khash, shift + 5, k, v).put(addedLeaf, hash, shift + 5, key, value)
            copy(storage = storage.clone().apply {
                this[2 * idx] = null
                this[2 * idx + 1] = node
            })
        }
    }
}
