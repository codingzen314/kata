package tech.codingzen.kata.either

import tech.codingzen.kata.tuple.*

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  block: (KTuple2<A, B>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.map { b -> block(KTuple(a, b)) }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  block: (KTuple3<A, B, C>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.map { c -> block(KTuple(a, b, c)) }
    }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param D partD type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [partD] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, D, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  partD: Either<L, D>,
  block: (KTuple4<A, B, C, D>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.bind { c ->
        partD.map { d -> block(KTuple(a, b, c, d)) }
      }
    }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param D partD type
 * @param E partE type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [partD] one of the parts to bind
 * @param [partE] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, D, E, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  partD: Either<L, D>,
  partE: Either<L, E>,
  block: (KTuple5<A, B, C, D, E>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.bind { c ->
        partD.bind { d ->
          partE.map { e -> block(KTuple(a, b, c, d, e)) }
        }
      }
    }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param D partD type
 * @param E partE type
 * @param F partF type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [partD] one of the parts to bind
 * @param [partE] one of the parts to bind
 * @param [partF] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, D, E, F, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  partD: Either<L, D>,
  partE: Either<L, E>,
  partF: Either<L, F>,
  block: (KTuple6<A, B, C, D, E, F>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.bind { c ->
        partD.bind { d ->
          partE.bind { e ->
            partF.map { f -> block(KTuple(a, b, c, d, e, f)) }
          }
        }
      }
    }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param D partD type
 * @param E partE type
 * @param F partF type
 * @param G partG type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [partD] one of the parts to bind
 * @param [partE] one of the parts to bind
 * @param [partF] one of the parts to bind
 * @param [partG] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, D, E, F, G, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  partD: Either<L, D>,
  partE: Either<L, E>,
  partF: Either<L, F>,
  partG: Either<L, G>,
  block: (KTuple7<A, B, C, D, E, F, G>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.bind { c ->
        partD.bind { d ->
          partE.bind { e ->
            partF.bind { f ->
              partG.map { g -> block(KTuple(a, b, c, d, e, f, g)) }
            }
          }
        }
      }
    }
  }

/**
 * @param L lpartFt type
 * @param A partA type
 * @param B partB type
 * @param C partC type
 * @param D partD type
 * @param E partE type
 * @param F partF type
 * @param G partG type
 * @param H partG type
 * @param R result type
 * @param [partA] one of the parts to bind
 * @param [partB] one of the parts to bind
 * @param [partC] one of the parts to bind
 * @param [partD] one of the parts to bind
 * @param [partE] one of the parts to bind
 * @param [partF] one of the parts to bind
 * @param [partG] one of the parts to bind
 * @param [partH] one of the parts to bind
 * @param [block] function that accepts all the part values and maps them to a result
 * @return the bind'ing of all the parts.  All parts must be Either right containers each holding a value to be passed
 * to [block]
 */
inline fun <L, A, B, C, D, E, F, G, H, R> bindAll(
  partA: Either<L, A>,
  partB: Either<L, B>,
  partC: Either<L, C>,
  partD: Either<L, D>,
  partE: Either<L, E>,
  partF: Either<L, F>,
  partG: Either<L, G>,
  partH: Either<L, H>,
  block: (KTuple8<A, B, C, D, E, F, G, H>) -> R
): Either<L, R> =
  partA.bind { a ->
    partB.bind { b ->
      partC.bind { c ->
        partD.bind { d ->
          partE.bind { e ->
            partF.bind { f ->
              partG.bind { g ->
                partH.map { h -> block(KTuple(a, b, c, d, e, f, g, h)) }
              }
            }
          }
        }
      }
    }
  }