package tech.codingzen.kata.list

import org.junit.Test
import tech.codingzen.kata.list.KataList.Companion.cons
import tech.codingzen.kata.list.KataList.Companion.katalist
import tech.codingzen.kata.list.KataList.Companion.nil
import kotlin.test.*

fun assertIsEmpty(list: KataList<*>) {
  assertTrue(list.isEmpty(), "Expected KataList to be empty")
}

fun assertIsNil(list: KataList<*>) {
  assertTrue(list.isNil, "Expected KataList to be nil")
}

fun assertIsNotNil(list: KataList<*>) {
  assertFalse(!list.isNil, "Expected KataList to not be nil")
}

fun assertIsCons(list: KataList<*>) {
  assertTrue(list.isCons, "Expected KataList to be nil")
}

fun assertIsNotCons(list: KataList<*>) {
  assertFalse(!list.isCons, "Expected KataList to not be nil")
}

fun assertIsNotEmpty(list: KataList<*>) {
  assertFalse(list.isEmpty(), "Expected KataList to not be empty")
}

fun assertSize(expected: Int, list: KataList<*>) {
  assertEquals(expected, list.size, "Expected KataList to have size of $expected")
}

fun <T> assertHead(expected: T, list: KataList<T>) {
  assertEquals(expected, list.head, "Expected head of KataList to be $expected")
}

fun <T> assertTail(expected: KataList<T>, list: KataList<T>) {
  assertEquals(expected, list.tail, "Expected head of KataList to be $expected")
}

fun <T> assertIndexOf(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOf(value), "Expected index of $value to be $expected")
}

fun <T> assertIndexOfFirst(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOfFirst(value), "Expected index of $value to be $expected")
}

fun <T> assertIndexOfLast(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOfLast(value), "Expected index of $value to be $expected")
}

class KataListTest {
  @Test
  fun sizeTest() {
    assertSize(0, nil<Any>())
    assertSize(1, cons("a"))
    assertSize(4, katalist("a", "b", "c", "d"))
  }

  @Test
  fun isEmptyTest() {
    assertIsEmpty(nil<Any>())
    assertIsNotEmpty(cons("a"))
    assertIsNotEmpty(katalist("a", "b", "c", "d"))
  }

  @Test
  fun headTest() {
    assertFailsWith(NoSuchElementException::class) { nil<Any>().head }
    assertHead(2, cons(2))
    assertHead("a", katalist("a", "b"))
  }

  @Test
  fun tailTest() {
    assertTail(nil(), nil<Any>())
    assertTail(nil(), cons(2))
    assertTail(katalist("b"), katalist("a", "b"))
  }

  @Test
  fun asConsTest() {
    assertFailsWith(IllegalStateException::class) { nil<Any>().asCons }
    cons(2).run { assertSame(this, asCons) }
  }

  @Test
  fun asNilTest() {
    assertIsNil(nil<Any>())
    assertFailsWith(IllegalStateException::class) { cons(1).asNil }
  }

  @Test
  fun indexOfTest() {
    assertIndexOf(-1, 0, nil<Any>())
    assertIndexOf(0, "a", katalist("a", "b", "a"))
    assertIndexOf(1, "b", katalist("a", "b", "a"))
  }

  @Test
  fun indexOfFirstTest() {
    assertIndexOfFirst(-1, 0, nil<Any>())
    assertIndexOfFirst(0, "a", katalist("a", "b", "a"))
    assertIndexOfFirst(1, "b", katalist("a", "b", "a"))
  }

  @Test
  fun indexOfLastTest() {
    assertIndexOfLast(-1, 0, nil<Any>())
    assertIndexOfLast(2, "a", katalist("a", "b", "a"))
    assertIndexOfLast(1, "b", katalist("a", "b", "a"))
    assertIndexOfLast(-1, "d", katalist("a", "b", "c"))
  }

  @Test
  fun invokeTest() {
    assertFailsWith(IndexOutOfBoundsException::class) { nil<Any>()(0) }
    assertEquals("a", katalist("a", "b")(0))
    assertEquals("b", katalist("a", "b")(1))
    assertFailsWith(IndexOutOfBoundsException::class) { katalist("a", "b")(2) }
  }

  @Test
  fun getTest() {
    assertFailsWith(IndexOutOfBoundsException::class) { nil<Any>()[0] }
    assertEquals("a", katalist("a", "b")[0])
    assertEquals("b", katalist("a", "b")[1])
    assertFailsWith(IndexOutOfBoundsException::class) { katalist("a", "b")[2] }
  }

  @Test
  fun subListTest() {
    assertIsNil(nil<Any>().subList(0, 0))
    assertIsNil(cons("a").subList(0, 0))
    assertEquals(katalist("a"), cons("a").subList(0, 1))
    assertEquals(katalist("a"), katalist("a", "b").subList(0, 1))
    assertEquals(katalist("a", "b"), katalist("a", "b", "c").subList(0, 2))
    assertEquals(katalist("b"), katalist("a", "b", "c").subList(1, 2))
    assertEquals(katalist("b", "c"), katalist("a", "b", "c", "d").subList(1, 3))
    assertEquals(katalist("a", "b", "c", "d"), katalist("a", "b", "c", "d").subList(0, 4))
  }

  @Test
  fun component1Test() {
    assertFailsWith(IndexOutOfBoundsException::class) { val (e0) = nil<Any>() }
    run { val (e0) = cons("a"); assertEquals("a", e0) }
    run { val (e0) = katalist("a", "b"); assertEquals("a", e0) }
  }

  @Test
  fun component2Test() {
    assertFailsWith(IndexOutOfBoundsException::class) { val (e0, e1) = cons("a") }
    run { val (e0, e1) = katalist("a", "b"); assertEquals("a", e0); assertEquals("b", e1) }
    run { val (e0, e1) = katalist("a", "b"); assertEquals("a", e0); assertEquals("b", e1) }
  }

  @Test
  fun component3Test() {
    assertFailsWith(IndexOutOfBoundsException::class) { val (e0, e1, e2) = katalist("a", "b") }
    run {
      val (e0, e1, e2) = katalist("a", "b", "c")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
    }
    run {
      val (e0, e1, e2) = katalist("a", "b", "c", "d")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
    }
  }

  @Test
  fun component4Test() {
    assertFailsWith(IndexOutOfBoundsException::class) { val (e0, e1, e2, e3) = katalist("a", "b", "c") }
    run {
      val (e0, e1, e2, e3) = katalist("a", "b", "c", "d")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
    }
    run {
      val (e0, e1, e2, e3) = katalist("a", "b", "c", "d", "e")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
    }
  }

  @Test
  fun component5Test() {
    assertFailsWith(IndexOutOfBoundsException::class) { val (e0, e1, e2, e3, e4) = katalist("a", "b", "c", "d") }
    run {
      val (e0, e1, e2, e3, e4) = katalist("a", "b", "c", "d", "e")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
    }
    run {
      val (e0, e1, e2, e3, e4) = katalist("a", "b", "c", "d", "e", "f")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
    }
  }

  @Test
  fun component6Test() {
    assertFailsWith(IndexOutOfBoundsException::class) {
      val (e0, e1, e2, e3, e4, e5) = katalist(
        "a",
        "b",
        "c",
        "d",
        "e"
      )
    }
    run {
      val (e0, e1, e2, e3, e4, e5) = katalist("a", "b", "c", "d", "e", "f")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
    }
    run {
      val (e0, e1, e2, e3, e4, e5) = katalist("a", "b", "c", "d", "e", "f", "g")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
    }
  }

  @Test
  fun component7Test() {
    assertFailsWith(IndexOutOfBoundsException::class) {
      val (e0, e1, e2, e3, e4, e5, e6) = katalist(
        "a",
        "b",
        "c",
        "d",
        "e",
        "f"
      )
    }
    run {
      val (e0, e1, e2, e3, e4, e5, e6) = katalist("a", "b", "c", "d", "e", "f", "g")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
      assertEquals("g", e6)
    }
    run {
      val (e0, e1, e2, e3, e4, e5, e6) = katalist("a", "b", "c", "d", "e", "f", "g", "h")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
      assertEquals("g", e6)
    }
  }

  @Test
  fun component8Test() {
    assertFailsWith(IndexOutOfBoundsException::class) {
      val (e0, e1, e2, e3, e4, e5, e6, e7) = katalist(
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g"
      )
    }
    run {
      val (e0, e1, e2, e3, e4, e5, e6, e7) = katalist("a", "b", "c", "d", "e", "f", "g", "h")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
      assertEquals("g", e6)
      assertEquals("h", e7)
    }
    run {
      val (e0, e1, e2, e3, e4, e5, e6, e7) = katalist("a", "b", "c", "d", "e", "f", "g", "h", "i")
      assertEquals("a", e0)
      assertEquals("b", e1)
      assertEquals("c", e2)
      assertEquals("d", e3)
      assertEquals("e", e4)
      assertEquals("f", e5)
      assertEquals("g", e6)
      assertEquals("h", e7)
    }
  }

  @Test
  fun minusTest() {
    assertIsNil(nil<Any>() - "a")
    assertIsNil(katalist("a", "a", "a") - "a")
    assertEquals(katalist("b"), katalist("a", "b") - "a")
    assertEquals(katalist("b", "c"), katalist("a", "b", "c") - "a")
    assertEquals(katalist("a", "b", "c"), katalist("a", "b", "c") - "d")
  }

  @Test
  fun iteratorTest() {
    nil<Any>().iterator().run {
      assertFalse(hasNext())
      assertFailsWith(NoSuchElementException::class) { next() }
    }

    katalist("a", "b").iterator().run {
      assertTrue(hasNext())
      assertEquals("a", next())
      assertTrue(hasNext())
      assertEquals("b", next())
      assertFalse(hasNext())
      assertFailsWith(NoSuchElementException::class) { next() }
    }
  }

  @Test
  fun reversedTest() {
    assertIsNil(nil<Any>().reversed())
    assertEquals(katalist(1), katalist(1).reversed())
    assertEquals(katalist(4, 3, 2, 1), katalist(1, 2, 3, 4).reversed())
  }

  @Test
  fun toStringTest() {
    assertEquals("()", nil<Any>().toString())
    assertEquals("(1)", katalist(1).toString())
    assertEquals("(1,2)", katalist(1, 2).toString())
  }

  @Test
  fun mapTest() {
    assertIsNil(nil<Any>().map { it.toString() })
    assertEquals(katalist(1, 2, 3), katalist("a", "bb", "ccc").map { it.length })
  }

  @Test
  fun flatMapTest() {
    assertIsNil(nil<Any>().flatMap { katalist(1, 2, 3) })
    assertEquals(katalist(1, 2, 1, 2, 1, 2), katalist("a", "bb", "ccc").flatMap { katalist(1, 2) })
    assertEquals(katalist(100, 200, 300), katalist("a", "bb", "ccc").flatMap {
      when (it) {
        "a" -> katalist(100)
        "bb" -> nil()
        else -> katalist(200, 300)
      }
    })
  }
}