package tech.codingzen.kata.tuple

import org.junit.Test
import java.util.*

class KTuple8Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "foo", 100, 'c', 21L, 1.toByte(), 2.71, false,
      "B", tuple.set0("foo")
    )
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", "foo", 'c', 21L, 1.toByte(), 2.71, false,
      "B", tuple.set1("foo")
    )
  }

  @Test
  fun test_set2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", 100, "foo", 21L, 1.toByte(), 2.71, false,
      "B", tuple.set2("foo")
    )
  }

  @Test
  fun test_set3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", 100, 'c', "foo", 1.toByte(), 2.71, false,
      "B", tuple.set3("foo")
    )
  }

  @Test
  fun test_set4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', 21L, "foo", 2.71, false, "B", tuple.set4("foo"))
  }

  @Test
  fun test_set5() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", 100, 'c', 21L, 1.toByte(), "foo", false,
      "B", tuple.set5("foo")
    )
  }

  @Test
  fun test_set6() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", 100, 'c', 21L, 1.toByte(), 2.71, "foo",
      "B", tuple.set6("foo")
    )
  }

  @Test
  fun test_set7() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals(
      "a", 100, 'c', 21L, 1.toByte(), 2.71, false,
      "foo", tuple.set7("foo")
    )
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("A", 100, 'c', 21L, 1.toByte(),
      2.71, false, "B", tuple.map0 { it.uppercase(Locale.getDefault()) })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 123, 'c', 21L, 1.toByte(), 2.71, false,
      "B", tuple.map1 { it + 23 })
  }

  @Test
  fun test_map2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'C', 21L, 1.toByte(),
      2.71, false, "B", tuple.map2 { it.uppercaseChar() })
  }

  @Test
  fun test_map3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', "a", 1.toByte(), 2.71, false,
      "B", tuple.map3 { "a" })
  }

  @Test
  fun test_map4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', 21L, 2, 2.71, false,
      "B", tuple.map4 { it + 1 })
  }

  @Test
  fun test_map5() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), "a", false, "B", tuple.map5 { "a" })
  }

  @Test
  fun test_map6() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), 2.71, true,
      "B", tuple.map6 { true })
  }

  @Test
  fun test_map7() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(),
      2.71, false, "b", tuple.map7 { it.lowercase(Locale.getDefault()) })
  }
}
