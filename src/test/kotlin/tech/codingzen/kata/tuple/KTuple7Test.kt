package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple7Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("foo", 100, 'c', 21L, 1.toByte(), 2.71, false, tuple.set0("foo"))
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", "foo", 'c', 21L, 1.toByte(), 2.71, false, tuple.set1("foo"))
  }

  @Test
  fun test_set2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, "foo", 21L, 1.toByte(), 2.71, false, tuple.set2("foo"))
  }

  @Test
  fun test_set3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', "foo", 1.toByte(), 2.71, false, tuple.set3("foo"))
  }

  @Test
  fun test_set4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, "foo", 2.71, false, tuple.set4("foo"))
  }

  @Test
  fun test_set5() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), "foo", false, tuple.set5("foo"))
  }

  @Test
  fun test_set6() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), 2.71, "foo", tuple.set6("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("A", 100, 'c', 21L, 1.toByte(),
      2.71, false, tuple.map0 { it.toUpperCase() })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 123, 'c', 21L, 1.toByte(), 2.71, false, tuple.map1 { it + 23 })
  }

  @Test
  fun test_map2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'C', 21L, 1.toByte(),
      2.71, false, tuple.map2 { it.toUpperCase() })
  }

  @Test
  fun test_map3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', "a", 1.toByte(), 2.71, false, tuple.map3 { "a" })
  }

  @Test
  fun test_map4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, 2, 2.71, false, tuple.map4 { it + 1 })
  }

  @Test
  fun test_map5() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), "a", false, tuple.map5 { "a" })
  }

  @Test
  fun test_map6() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertKataTupleEquals("a", 100, 'c', 21L, 1.toByte(), 2.71, true, tuple.map6 { true })
  }
}
