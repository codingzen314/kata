package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple1Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a")
    assertKataTupleEquals("foo", tuple.set0("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a")
    assertKataTupleEquals("A", tuple.map0 { it.uppercase() })
  }
}