package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple5Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("foo", 100, 'c', 21L, 1.toByte(), tuple.set0("foo"))
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", "foo", 'c', 21L, 1.toByte(), tuple.set1("foo"))
  }

  @Test
  fun test_set2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, "foo", 21L, 1.toByte(), tuple.set2("foo"))
  }

  @Test
  fun test_set3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, 'c', "foo", 1.toByte(), tuple.set3("foo"))
  }

  @Test
  fun test_set4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, 'c', 21L, "foo", tuple.set4("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("A", 100, 'c', 21L, 1.toByte(), tuple.map0 { it.toUpperCase() })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 123, 'c', 21L, 1.toByte(), tuple.map1 { it + 23 })
  }

  @Test
  fun test_map2() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, 'C', 21L, 1.toByte(), tuple.map2 { it.toUpperCase() })
  }

  @Test
  fun test_map3() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, 'c', "a", 1.toByte(), tuple.map3 { "a" })
  }

  @Test
  fun test_map4() {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte())
    assertKataTupleEquals("a", 100, 'c', 21L, 2, tuple.map4 { it + 1 })
  }
}
