package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple4Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("foo", 100, 'c', 21L, tuple.set0("foo"))
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", "foo", 'c', 21L, tuple.set1("foo"))
  }

  @Test
  fun test_set2() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", 100, "foo", 21L, tuple.set2("foo"))
  }

  @Test
  fun test_set3() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", 100, 'c', "foo", tuple.set3("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("A", 100, 'c', 21L, tuple.map0 { it.toUpperCase() })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", 123, 'c', 21L, tuple.map1 { it + 23 })
  }

  @Test
  fun test_map2() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", 100, 'C', 21L, tuple.map2 { it.toUpperCase() })
  }

  @Test
  fun test_map3() {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertKataTupleEquals("a", 100, 'c', "a", tuple.map3 { "a" })
  }
}
