package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple3Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("foo", 100, 'c', tuple.set0("foo"))
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("a", "foo", 'c', tuple.set1("foo"))
  }

  @Test
  fun test_set2() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("a", 100, "foo", tuple.set2("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("A", 100, 'c', tuple.map0 { it.toUpperCase() })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("a", 123, 'c', tuple.map1 { it + 23 })
  }

  @Test
  fun test_map2() {
    val tuple = KTuple("a", 100, 'c')
    assertKataTupleEquals("a", 100, 'C', tuple.map2 { it.toUpperCase() })
  }
}
