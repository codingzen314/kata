package tech.codingzen.kata.tuple

import org.junit.Test

class KTuple2Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a", 100)
    assertKataTupleEquals("foo", 100, tuple.set0("foo"))
  }

  @Test
  fun test_set1() {
    val tuple = KTuple("a", 100)
    assertKataTupleEquals("a", "foo", tuple.set1("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a", 100)
    assertKataTupleEquals("A", 100, tuple.map0 { it.toUpperCase() })
  }

  @Test
  fun test_map1() {
    val tuple = KTuple("a", 100)
    assertKataTupleEquals("a", 123, tuple.map1 { it + 23 })
  }
}
