package tech.codingzen.kata.tuple

import kotlin.test.assertEquals

fun <P0> assertKataTupleEquals(part0: P0, actual: KTuple1<P0>) {
  assertEquals(KTuple(part0), actual)
}

fun <P0, P1> assertKataTupleEquals(part0: P0, part1: P1, actual: KTuple2<P0, P1>) {
  assertEquals(KTuple2(part0, part1), actual)
}

fun <P0, P1, P2> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  actual: KTuple3<P0, P1, P2>
) {
  assertEquals(KTuple3(part0, part1, part2), actual)
}

fun <P0, P1, P2, P3> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  part3: P3,
  actual: KTuple4<P0, P1, P2, P3>
) {
  assertEquals(KTuple4(part0, part1, part2, part3), actual)
}

fun <P0, P1, P2, P3, P4> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  part3: P3,
  part4: P4,
  actual: KTuple5<P0, P1, P2, P3, P4>
) {
  assertEquals(KTuple5(part0, part1, part2, part3, part4), actual)
}

fun <P0, P1, P2, P3, P4, P5> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  part3: P3,
  part4: P4,
  part5: P5,
  actual: KTuple6<P0, P1, P2, P3, P4, P5>
) {
  assertEquals(KTuple6(part0, part1, part2, part3, part4, part5), actual)
}

fun <P0, P1, P2, P3, P4, P5, P6> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  part3: P3,
  part4: P4,
  part5: P5,
  part6: P6,
  actual: KTuple7<P0, P1, P2, P3, P4, P5, P6>
) {
  assertEquals(KTuple7(part0, part1, part2, part3, part4, part5, part6), actual)
}

fun <P0, P1, P2, P3, P4, P5, P6, P7> assertKataTupleEquals(
  part0: P0,
  part1: P1,
  part2: P2,
  part3: P3,
  part4: P4,
  part5: P5,
  part6: P6,
  part7: P7,
  actual: KTuple8<P0, P1, P2, P3, P4, P5, P6, P7>
) {
  assertEquals(KTuple8(part0, part1, part2, part3, part4, part5, part6, part7), actual)
}