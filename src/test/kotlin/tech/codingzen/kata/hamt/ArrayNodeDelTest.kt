package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertNull

class ArrayNodeDelTest {
  @Test
  fun `del size 1 found`() = NodeTestSyntax {
    val root = arrn(1, hashn(tkey("a"), "b"), null)
    val entry = root.del(tkey("a"))
    assertNull(entry)
  }

  @Test
  fun `del not found`() = NodeTestSyntax {
    val root = arrn(1, hashn("a", "b"), null)
    val entry = root.del(tkey("nope", p0 = 1))
    assertEquals(root, entry)
  }

  @Test
  fun `del size 2 found`() = NodeTestSyntax {
    val kA = tkey("a")
    val vA = "vA"
    val kB = tkey("b", p0 = 1)
    val vB = "vB"
    val root = arrn(2, hashn(kA, vA), hashn(kB, vB))
    val node = root.del(kB)
    assertEquals(arrn(1, hashn(kA, vA), null), node)
  }

  @Test
  fun `del size 1 child`() = NodeTestSyntax {
    val kA = tkey("a")
    val vA = "vA"
    val kB = tkey("b")
    val vB = "vB"
    val root = arrn(1, hashn(kA, vA, kB, vB))
    val node = root.del(kB)
    assertEquals(arrn(1, hashn(kA, vA)), node)
  }
}