package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class HashCollisionNodeIteratorTest {
  @Test
  fun `single`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"

    val iter = hashn(kA, vA).iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(kA, vA), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `multiple kv's`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val kB = tkey("b", 2)
    val vB = "vb"

    val iter = hashn(kA, vA, kB, vB).iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(kA, vA), iter.next())
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(kB, vB), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

}