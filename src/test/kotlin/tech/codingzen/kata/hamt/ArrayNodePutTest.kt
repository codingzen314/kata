package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.*

class ArrayNodePutTest {
  @Test
  fun `put in empty`() {
    val keyA = tkey("a", p0 = 2)
    val valA = "va"
    val node = ArrayNode(0, arrayOfNulls(32)).put(AddedLeaf(), keyA.hash, 0, keyA, valA)
    assertIs<ArrayNode>(node)
    val an = node as ArrayNode
    assertEquals(1, an.count)
    assertNotNull(an.nodes[2])
    assertEquals(PartialNode(1, arrayOf(keyA, valA)), an.nodes[2])
  }

  @Test
  fun `put in child node`() {
    val keyA = tkey("a", p0 = 2)
    val valA = "va"
    val keyB = tkey("b", p1 = 3, p0 = 2)
    val valB = "vb"
    val addedLeaf = AddedLeaf()
    val root = ArrayNode(0, arrayOfNulls(32)).put(addedLeaf, keyA.hash, 0, keyA, valA)
    val node = root.put(addedLeaf, keyB.hash, 0, keyB, valB)
    assertTrue(addedLeaf.added)
    assertIs<ArrayNode>(node)
    val an = node as ArrayNode
    assertEquals(1, an.count)
    assertNotNull(an.nodes[2])
    assertEquals(PartialNode(0b1001, arrayOf(keyA, valA, keyB, valB)), an.nodes[2])
  }

  @Test
  fun `put existing`() {
    val keyA = tkey("a", p0 = 2)
    val valA = "va"
    val addedLeaf = AddedLeaf()
    val root = ArrayNode(0, arrayOfNulls(32)).put(AddedLeaf(), keyA.hash, 0, keyA, valA)
    val node = root.put(addedLeaf, keyA.hash, 0, keyA, valA)
    assertFalse(addedLeaf.added)
    assertSame(root, node)
  }

  @Test
  fun `update existing`() {
    val keyA = tkey("a", p0 = 2)
    val valA = "va"
    val valA1 = "vA"
    val addedLeaf = AddedLeaf()
    val root = ArrayNode(0, arrayOfNulls(32)).put(AddedLeaf(), keyA.hash, 0, keyA, valA)
    val node = root.put(addedLeaf, keyA.hash, 0, keyA, valA1)
    assertFalse(addedLeaf.added)
    assertIs<ArrayNode>(node)
    val an = node as ArrayNode
    assertEquals(1, an.count)
    assertNotNull(an.nodes[2])
    assertEquals(PartialNode(1, arrayOf(keyA, valA1)), an.nodes[2])
  }
}
