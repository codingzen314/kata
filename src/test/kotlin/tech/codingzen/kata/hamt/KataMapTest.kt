package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.tuple.KTuple
import kotlin.test.*

class KataMapTest {
  @Test
  fun `empty test`() {
    val kmap = KataMap.empty<String, Int>()
    assertTrue(kmap.isEmpty())
    assertFalse(kmap.isNotEmpty())
    assertEquals(0, kmap.size)
    assertNull(kmap["nope"])
    assertEquals(kmap, kmap.del("none"))
    assertEquals(kmap, kmap - "none")
    assertNotEquals(kmap, kmap.put("a", 1))
    assertNotEquals(kmap, kmap + ("a" to 1))
    assertFalse(kmap.containsKey("none"))
    assertFalse(kmap.containsValue(2))
    val entries = kmap.entries
    assertEquals(0, entries.size)
    assertFalse(entries.iterator().hasNext())
    val keys = kmap.keys
    assertEquals(0, keys.size)
    assertFalse(keys.iterator().hasNext())
    val values = kmap.values
    assertEquals(0, values.size)
    assertFalse(values.iterator().hasNext())
    assertEquals("{}", kmap.toString())
  }

  @Test
  fun `single test`() {
    val kmap = KataMap("a" to 0)
    assertFalse(kmap.isEmpty())
    assertTrue(kmap.isNotEmpty())
    assertEquals(1, kmap.size)
    assertNull(kmap["nope"])
    assertEquals(0, kmap["a"])
    assertEquals(0 to null, kmap["a", "b"])
    assertEquals(Triple(0, null, null), kmap["a", "b", "c"])
    assertEquals(KTuple(0, null, null, null), kmap["a", "b", "c", "d"])
    assertEquals(KTuple(0, null, null, null, null), kmap["a", "b", "c", "d", "e"])
    assertEquals(KTuple(0, null, null, null, null, null), kmap["a", "b", "c", "d", "e", "f"])
    assertEquals(KTuple(0, null, null, null, null, null, null), kmap["a", "b", "c", "d", "e", "f", "g"])
    assertEquals(KTuple(0, null, null, null, null, null, null, null), kmap["a", "b", "c", "d", "e", "f", "g", "h"])
    assertEquals(kmap, kmap.del("none"))
    assertEquals(KataMap.empty(), kmap.del("a"))
    assertEquals(KataMap.empty(), kmap - "a")
    assertNotEquals(kmap, kmap.put("a", 1))
    assertEquals(1, kmap.put("a", 1)["a"])
    assertFalse(kmap.containsKey("none"))
    assertTrue(kmap.containsKey("a"))
    assertFalse(kmap.containsValue(2))
    assertTrue(kmap.containsValue(0))
    val entries = kmap.entries
    assertEquals(1, entries.size)
    val entriesIterator = entries.iterator()
    assertTrue(entriesIterator.hasNext())
    entriesIterator.next().let { (k, v) ->
      assertEquals("a", k)
      assertEquals(0, v)
    }
    assertFalse(entriesIterator.hasNext())
    val keys = kmap.keys
    assertEquals(1, keys.size)
    val keysIterator = keys.iterator()
    assertTrue(keysIterator.hasNext())
    assertEquals("a", keysIterator.next())
    assertFalse(keysIterator.hasNext())
    val values = kmap.values
    assertEquals(1, values.size)
    val valuesIterator = values.iterator()
    assertTrue(valuesIterator.hasNext())
    assertEquals(0, valuesIterator.next())
    assertFalse(valuesIterator.hasNext())
    assertEquals("{(a,0)}", kmap.toString())
  }

  @Test
  fun `multiple test`() {
    val kmap = KataMap("a" to 0) + ("b" to 1).toEntry()
    assertFalse(kmap.isEmpty())
    assertTrue(kmap.isNotEmpty())
    assertEquals(2, kmap.size)
    assertNull(kmap["nope"])
    assertEquals(0, kmap["a"])
    assertEquals(1, kmap["b"])
    assertEquals(kmap, kmap.del("none"))
    assertEquals(KataMap("b" to 1), kmap.del("a"))
    assertEquals(KataMap("a" to 0), kmap.del("b"))
    assertEquals(KataMap.empty(), kmap - "a" - "b")
    assertNotEquals(kmap, kmap.put("a", 1))
    assertEquals(1, kmap.put("a", 1)["a"])
    assertFalse(kmap.containsKey("none"))
    assertTrue(kmap.containsKey("a"))
    assertTrue(kmap.containsKey("b"))
    assertFalse(kmap.containsValue(2))
    assertTrue(kmap.containsValue(1))
    assertTrue(kmap.containsValue(0))
    val entries = kmap.entries
    assertEquals(2, entries.size)
    val entriesIterator = entries.iterator()
    assertTrue(entriesIterator.hasNext())
    entriesIterator.next().let { (k, v) ->
      assertEquals("a", k)
      assertEquals(0, v)
    }
    assertTrue(entriesIterator.hasNext())
    entriesIterator.next().let { (k, v) ->
      assertEquals("b", k)
      assertEquals(1, v)
    }
    assertFalse(entriesIterator.hasNext())
    val keys = kmap.keys
    assertEquals(2, keys.size)
    val keysIterator = keys.iterator()
    assertTrue(keysIterator.hasNext())
    assertEquals("a", keysIterator.next())
    assertTrue(keysIterator.hasNext())
    assertEquals("b", keysIterator.next())
    assertFalse(keysIterator.hasNext())
    val values = kmap.values
    assertEquals(2, values.size)
    val valuesIterator = values.iterator()
    assertTrue(valuesIterator.hasNext())
    assertEquals(0, valuesIterator.next())
    assertTrue(valuesIterator.hasNext())
    assertEquals(1, valuesIterator.next())
    assertFalse(valuesIterator.hasNext())
    assertEquals("{(a,0),(b,1)}", kmap.toString())
  }

  @Test
  fun test_multiple_get() {
    val map1 = KataMap("a" to 0)
    val map2 = map1 + ("b" to 1)
    val map3 = map2 + ("c" to 2)
    val map4 = map3 + ("d" to 3)
    val map5 = map4 + ("e" to 4)
    val map6 = map5 + ("f" to 5)
    val map7 = map6 + ("g" to 6)
    val map8 = map7 + ("h" to 7)
    assertEquals(0, map1["a"])
    assertEquals(0 to 1, map2["a", "b"])
    assertEquals(Triple(0, 1, 2), map3["a", "b", "c"])
    assertEquals(KTuple(0, 1, 2, 3), map4["a", "b", "c", "d"])
    assertEquals(KTuple(0, 1, 2, 3, 4), map5["a", "b", "c", "d", "e"])
    assertEquals(KTuple(0, 1, 2, 3, 4, 5), map6["a", "b", "c", "d", "e", "f"])
    assertEquals(KTuple(0, 1, 2, 3, 4, 5, 6), map7["a", "b", "c", "d", "e", "f", "g"])
    assertEquals(KTuple(0, 1, 2, 3, 4, 5, 6, 7), map8["a", "b", "c", "d", "e", "f", "g", "h"])
  }
}

fun <K, V> Pair<K, V>.toEntry() = object : Map.Entry<K, V> {
  override val key: K
    get() = this@toEntry.first
  override val value: V
    get() = this@toEntry.second
}