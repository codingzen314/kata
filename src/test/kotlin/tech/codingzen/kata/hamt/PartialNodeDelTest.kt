package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertSame

class PartialNodeDelTest {
  @Test
  fun `del not found`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val root = partn(0).put(AddedLeaf(), kA, vA)
    val node = root.del("nope")
    assertSame(node, root)
    assertSame(root, root.del(tkey("A", p0 = 2)))
  }

  @Test
  fun `del single`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val root = partn(0).put(AddedLeaf(), kA, vA)
    val node = root.del(kA)
    assertNull(node)
  }

  @Test
  fun `del multiple`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 3)
    val vB = "vB"
    val root = partn(0).put(AddedLeaf(), kA, vA).put(AddedLeaf(), kB, vB)
    assertEquals(partn(0b0100, kA, vA), root.del(kB))
  }

  @Test
  fun `del single child`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 3)
    val vB = "vB"
    val node = partn(0b01100, kA, vA, null, hashn(kB, vB))
    assertEquals(partn(0b0100, kA, vA), node.del(kB))
  }

  @Test
  fun `del single child multiple`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 3)
    val vB = "vB"
    val kC = tkey("c", p0 = 3)
    val vC = "vC"
    val node = partn(0b01100, kA, vA, null, hashn(kB, vB, kC, vC))
    assertEquals(partn(0b01100, kA, vA, null, hashn(kC, vC)), node.del(kB))
  }
}