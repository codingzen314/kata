package tech.codingzen.kata.hamt

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class ArrayNodeGetTest {
  @Test
  fun `get empty`() {
    val keyA = TKey.tkey("a", p0 = 0)
    val valA = "va"
    val root = ArrayNode(0, arrayOf())
    val entry = root.get(0, 0, "nope")
    assertNull(entry)
  }

  @Test
  fun `get single`() {
    val keyA = TKey.tkey("a", 2)
    val valA = "va"

    val root = ArrayNode(
      1, arrayOf(
        HashCollisionNode(hash(2), 0, arrayOfNulls(0))
          .put(AddedLeaf(), keyA.hash, 5, keyA, valA)
      )
    )
    val entry = root.get(0, 0, keyA)
    assertNotNull(entry)
    val (k, v) = entry!!
    assertEquals(keyA, k)
    assertEquals(valA, v)

    assertNull(root.get(0, 0, "nope"))
  }

  @Test
  fun `get multiple`() {
    val keys = (0 until 16).map { TKey.tkey("k$it", p0 = it) }
    val vals = (0 until 16).map { "v$it" }

    val storage: Array<HamtNode?> = arrayOfNulls(32)
    for ((index, x) in keys.zip(vals).withIndex()) {
      storage[index] = HashCollisionNode(0, 1, arrayOf(x.first, x.second))
    }
    val root = ArrayNode(keys.size, storage)
    for ((k, v) in keys.zip(vals)) {
      val entry = root.get(k.hash, 0, k)
      assertNotNull(entry)
      val (ak, av) = entry!!
      assertEquals(k, ak)
      assertEquals(v, av)
    }

    assertNull(root.get(17, 0, "nope"))
  }

}