package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PartialNodeIteratorTest {
  @Test
  fun `empty`() = NodeTestSyntax {
    val iter = partn(0).iterator()
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `single kv`() = NodeTestSyntax {
    val keyA = tkey("a", 2)
    val valA = "va"

    val iter = partn(0)
      .put(AddedLeaf(), keyA, valA)
      .iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(keyA, valA), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `single hashnode`() = NodeTestSyntax {
    val keyA = tkey("a", 2)
    val valA = "va"

    val iter = partn(0, null, HashCollisionNode(keyA.hash, 1, arrayOf(keyA, valA))).iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(keyA, valA), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `two kv and hashnode`() = NodeTestSyntax {
    val keyA = tkey("a", p0 = 2)
    val valA = "va"
    val keyB = tkey("b", p0 = 3)
    val valB = "vb"

    val iter = partn(3, null, HashCollisionNode(keyA.hash, 1, arrayOf(keyA, valA)), keyB, valB).iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(keyA, valA), iter.next())
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(keyB, valB), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `all 16`() = NodeTestSyntax {
    val keys = (0 until 16).map { tkey("a", p1 = it, p0 = it) }
    val vals = (0 until 16).map { "v$it" }

    val kvs = (0 until 16)
      .map { null to HashCollisionNode(keys[it].hash, 1, arrayOf(keys[it], vals[it])) }
      .flatMap { listOf(it.first, it.second) }
      .toTypedArray()
    val root = partn(32, *kvs)
    val iter = root.iterator()
    for (i in 0 until 16) {
      assertTrue(iter.hasNext())
      assertEquals(InternalEntry(keys[i], vals[i]), iter.next())
    }
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `all 16 mix`() = NodeTestSyntax {
    val keys = (0 until 16).map { tkey("a", p1 = it, p0 = it) }
    val vals = (0 until 16).map { "v$it" }

    val kvs = mutableListOf<Any?>().apply {
      addAll((0 until 8).map { null to HashCollisionNode(keys[it].hash, 1, arrayOf(keys[it], vals[it])) }
        .flatMap { listOf(it.first, it.second) })
      addAll((8 until 16).map { keys[it] to vals[it] }.flatMap { listOf(it.first, it.second) })
    }.toTypedArray()
    val root = partn(32, *kvs)
    val iter = root.iterator()
    for (i in 0 until 16) {
      assertTrue(iter.hasNext())
      assertEquals(InternalEntry(keys[i], vals[i]), iter.next())
    }
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

}