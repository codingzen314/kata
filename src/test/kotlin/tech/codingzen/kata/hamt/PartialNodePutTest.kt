package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.*

class PartialNodePutTest {
  @Test
  fun `put single`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val root = partn(0)
    val addedLeaf = AddedLeaf()
    val node = root.put(addedLeaf, kA, vA)
    assertEquals(partn(0b0100, kA, vA), node)
    assertTrue(addedLeaf.added)
  }

  @Test
  fun `put existing`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val root = partn(0).put(AddedLeaf(), kA, vA)
    val addedLeaf = AddedLeaf()
    val node = root.put(addedLeaf, kA, vA)
    assertSame(root, node)
    assertFalse(addedLeaf.added)
  }

  @Test
  fun `update existing`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val vA1 = "vB"
    val root = partn(0).put(AddedLeaf(), kA, vA)
    val addedLeaf = AddedLeaf()
    val node = root.put(addedLeaf, kA.hash, 0, kA, vA1)
    assertEquals(partn(0b0100, kA, vA1), node)
    assertFalse(addedLeaf.added)
  }

  @Test
  fun `put hashes same`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 2)
    val vB = "vB"
    val addedLeaf = AddedLeaf()
    val node = partn(0)
      .put(addedLeaf, kA, vA)
      .put(addedLeaf, kB, vB)
    assertEquals(partn(0b0100, null, HashCollisionNode(kA.hash, 2, arrayOf(kA, vA, kB, vB))), node)
    assertTrue(addedLeaf.added)
  }

  @Test
  fun `put hashes diff`() = NodeTestSyntax {
    val kA = tkey("a", p1 = 1, p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p1 = 3, p0 = 2)
    val vB = "vB"
    val addedLeaf = AddedLeaf()
    val node = partn(0)
      .put(addedLeaf, kA, vA)
      .put(addedLeaf, kB, vB)
    assertEquals(partn(0b0100, null, PartialNode(0b01010, arrayOf(kA, vA, kB, vB))), node)
    assertTrue(addedLeaf.added)
  }

  @Test
  fun `update existing child node`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 2)
    val vB = "vB"
    val vB1 = "vC"
    val addedLeaf = AddedLeaf()
    val node = partn(0)
      .put(AddedLeaf(), kA, vA)
      .put(AddedLeaf(), kB, vB)
      .put(addedLeaf, kB, vB1)
    assertEquals(partn(0b0100, null, HashCollisionNode(kA.hash, 2, arrayOf(kA, vA, kB, vB1))), node)
    assertFalse(addedLeaf.added)
  }

  @Test
  fun `put existing child node`() = NodeTestSyntax {
    val kA = tkey("a", p0 = 2)
    val vA = "vA"
    val kB = tkey("b", p0 = 2)
    val vB = "vB"
    val addedLeaf = AddedLeaf()
    val root = partn(0)
      .put(AddedLeaf(), kA, vA)
      .put(AddedLeaf(), kB, vB)
    val node = root.put(addedLeaf, kB, vB)
    assertSame(node, root)
    assertFalse(addedLeaf.added)
  }

  @Test
  fun `put count gte 16`() = NodeTestSyntax {
    val keys = (0 until 16).map { tkey("k$it", p1 = it, p0 = it) }
    val vals = (0 until 16).map { "v$it" }
    val kZ = tkey("z", p1 = 1, p0 = 16)
    val vZ = "vz"
    val root = keys.zip(vals).fold(partn(0) as HamtNode) { node, (k, v) -> node.put(AddedLeaf(), k, v) }
    val addedLeaf = AddedLeaf()
    val node = root.put(addedLeaf, kZ.hashCode(), 0, kZ, vZ)
    assertIs<ArrayNode>(node)
    assertTrue(addedLeaf.added)
    //TODO: check contents of ArrayNode
  }
}