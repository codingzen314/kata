package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class HashCollisionNodePutTest {
  @Test
  fun `same hash`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val kB = tkey("b", 2)
    val vB = "vb"

    val addedLeaf = AddedLeaf()
    val node = hashn(kA, vA).put(addedLeaf, kB, vB, 5)
    assertEquals(hashn(kA, vA, kB, vB), node)
    assertTrue(addedLeaf.added)
  }

  @Test
  fun `override value`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val vB = "vb"

    val addedLeaf = AddedLeaf()
    val node = hashn(kA, vA)
      .put(addedLeaf, kA, vB)
    assertEquals(hashn(kA, vB), node)
    assertFalse(addedLeaf.added)
  }

  @Test
  fun `different hash`() = NodeTestSyntax {
    val kA = tkey("a", p1 = 2, p0 = 0)
    val vA = "va"
    val kB = tkey("b", p1 = 1, p0 = 0)
    val vB = "vb"

    val hcn = hashn(kA, vA)
    val addedLeaf = AddedLeaf()
    val node = hcn.put(addedLeaf, kB, vB, 5)
    assertTrue(addedLeaf.added)
    assertEquals(partn(0b0110, kB, vB, null, hcn), node)
  }

  @Test
  fun `put existing`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"

    val root = hashn(kA, vA)
    val addedLeaf = AddedLeaf()
    val node = root.put(addedLeaf, kA, vA)
    assertFalse(addedLeaf.added)
    assertSame(root, node)
  }
}