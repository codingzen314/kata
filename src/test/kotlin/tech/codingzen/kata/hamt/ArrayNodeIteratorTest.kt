package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ArrayNodeIteratorTest {
  @Test
  fun `empty`() {
    val iter = ArrayNode(0, arrayOfNulls(32)).iterator()
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `single`() {
    val keyA = tkey("a", 2)
    val valA = "va"

    val iter = ArrayNode(1, arrayOfNulls<HamtNode>(32).apply {
      this[0] = HashCollisionNode(hash(2), 1, arrayOf(keyA, valA))
    }).iterator()
    assertTrue(iter.hasNext())
    assertEquals(InternalEntry(keyA, valA), iter.next())
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

  @Test
  fun `all 32`() {
    val keys = (0 until 32).map { tkey("a", p0 = it) }
    val vals = (0 until 32).map { "v$it" }

    val root = ArrayNode(32, arrayOfNulls<HamtNode>(32).apply {
      (0 until 32).forEach { this[it] = HashCollisionNode(keys[it].hash, 1, arrayOf(keys[it], vals[it])) }
    })
    val iter = root.iterator()
    for (i in 0 until 32) {
      assertTrue(iter.hasNext())
      assertEquals(InternalEntry(keys[i], vals[i]), iter.next())
    }
    assertFalse(iter.hasNext())
    assertFails { iter.next() }
  }

}