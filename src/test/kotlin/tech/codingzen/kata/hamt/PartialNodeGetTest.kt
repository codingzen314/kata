package tech.codingzen.kata.hamt

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class PartialNodeGetTest {
  @Test
  fun `get empty`() = NodeTestSyntax {
    val root = partn(0)
    val entry = root.get(0, 0, "nope")
    assertNull(entry)
  }

  @Test
  fun `get single`() {
    val keyA = TKey.tkey("a")
    val valA = "va"

    val root = HamtNode.single(keyA.hash, 0, keyA, valA)
      .put(AddedLeaf(), keyA.hash, 5, keyA, valA)
    val entry = root.get(0, 0, keyA)
    assertEquals(InternalEntry(keyA, valA), entry)
    assertNull(root.get(1, 0, "nope"))
  }

  @Test
  fun `get multiple`() = NodeTestSyntax {
    val keys = (0 until 16).map { TKey.tkey("k$it", p0 = it) }
    val vals = (0 until 16).map { "v$it" }


    val root = keys.zip(vals).fold(partn(0) as HamtNode) { acc, (k, v) -> acc.put(AddedLeaf(), k.hash, 0, k, v) }
    for ((k, v) in keys.zip(vals)) {
      val entry = root.get(k.hash, 0, k)
      assertEquals(InternalEntry(k, v), entry)
    }

    assertNull(root.get(17, 0, "nope"))
  }

  @Test
  fun `get single child`() = NodeTestSyntax {
    val keyA = TKey.tkey("a")
    val valA = "va"

    val root = partn(1, null, HashCollisionNode(0, 1, arrayOf(keyA, valA)))
    val entry = root.get(0, 0, keyA)
    assertNotNull(entry)
    assertNull(root.get(1, 0, "nope"))
  }
}