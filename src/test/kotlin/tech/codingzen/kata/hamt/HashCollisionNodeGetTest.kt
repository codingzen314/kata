package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class HashCollisionNodeGetTest {
  @Test
  fun `get single`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"

    val node = hashn(kA, vA)
    val entry = node.get(kA, 0)
    assertEquals(InternalEntry(kA, vA), entry)
    assertNull(node.get("nope", 0))
  }

  @Test
  fun `get multiple`() = NodeTestSyntax {
    val keys = (0 until 16).map { tkey("k$it") }
    val vals = (0 until 16).map { "v$it" }

    val node = hashn(*keys.zip(vals).flatMap { listOf(it.first, it.second) }.toTypedArray<Any?>())

    for ((k, v) in keys.zip(vals)) {
      val entry = node.get(k)
      assertNotNull(entry)
      val (ak, av) = entry!!
      assertEquals(k, ak)
      assertEquals(v, av)
    }
  }

}