package tech.codingzen.kata.hamt

import tech.codingzen.kata.hamt.HamtNode.Companion.bitpos

object NodeTestSyntax {
  operator fun invoke(block: NodeTestSyntax.() -> Unit) {
    NodeTestSyntax.apply(block)
  }

  object HashN {
    operator fun invoke(block: HashNSyntax.() -> Unit): HashCollisionNode =
      HashNSyntax().apply(block).node

    fun single(k: Any, v: Any?): HashCollisionNode =
      HashCollisionNode(k.hashCode(), 1, arrayOf(k, v))
  }

  object ArrN

  object PartN {
    operator fun invoke(bitmap: Int, vararg kvs: Any?) = PartialNode(bitmap, kvs.toList().toTypedArray())
  }

  fun hashn(vararg kvs: Any?): HashCollisionNode = when {
    kvs.isEmpty() -> throw IllegalStateException("HashCollisionNode must have at least one element")
    else -> HashCollisionNode(kvs[0].hashCode(), kvs.size / 2, kvs as Array<Any?>)
  }

  fun partn(bitmap: Int, vararg kvs: Any?) = PartialNode(bitmap, kvs as Array<Any?>)

  fun arrn(count: Int, vararg kvs: HamtNode?) = ArrayNode(count, kvs as Array<HamtNode?>)

  fun HamtNode.put(addedLeaf: AddedLeaf, k: TKey, v: Any?, shift: Int = 0): HamtNode =
    put(addedLeaf, k.hash, shift, k, v)

  fun HamtNode.del(k: Any, shift: Int = 0): HamtNode? = del(k.hashCode(), shift, k)
  fun HamtNode.get(k: Any, shift: Int = 0): InternalEntry? = get(k.hashCode(), shift, k)
}

class PartialNSyntax(val shift: Int) {
  private var kvs = mutableListOf<Pair<Any?, Any?>>()

  operator fun TKey.plus(v: Any?) {
    kvs.add(this to v)
  }

  val node: PartialNode
    get() {
      val bitmap = kvs.fold(0) { bitm, (k, v) ->
        bitm or bitpos(k.hashCode(), shift)
      }
      kvs.sortBy { bitpos(it.first.hashCode(), shift) }
      return PartialNode(bitmap, kvs.flatMap { listOf(it.first, it.second) }.toTypedArray())
    }
}

class HashNSyntax {
  private var kvs = mutableListOf<Pair<Any?, Any?>>()
  var hash: Int? = -1

  operator fun TKey.plus(v: Any?) {
    if (kvs.isEmpty()) this@HashNSyntax.hash = this.hash
    kvs.add(this to v)
  }

  val node: HashCollisionNode
    get() =
      if (kvs.isEmpty()) throw IllegalStateException("HashCollisionNode must exist with at least one element")
      else HashCollisionNode(hash!!, kvs.size, kvs.flatMap { listOf(it.first, it.second) }.toTypedArray())
}

data class TKey(val hash: Int, val name: String) {
  override fun hashCode(): Int = hash
  override fun equals(other: Any?): Boolean = other is TKey && name == other.name
  override fun toString(): String = name //"TKey($name)"

  companion object {
    fun tkey(
      name: String,
      p6: Int = 0,
      p5: Int = 0,
      p4: Int = 0,
      p3: Int = 0,
      p2: Int = 0,
      p1: Int = 0,
      p0: Int = 0
    ): TKey {
      val hash = (p6 shl 30) or (p5 shl 25) or (p4 shl 20) or (p3 shl 15) or (p2 shl 10) or (p1 shl 5) or p0
      return TKey(hash, name)
    }
  }
}

fun hash(p6: Int = 0, p5: Int = 0, p4: Int = 0, p3: Int = 0, p2: Int = 0, p1: Int = 0, p0: Int = 0) =
  (p6 shl 30) or (p5 shl 25) or (p4 shl 20) or (p3 shl 15) or (p2 shl 10) or (p1 shl 5) or p0

val Int.binstr: String
  get() = StringBuilder().apply {
    append(showBits(this@binstr ushr 30 and 0x01f, 2))
    append('_')
    append(showBits(this@binstr ushr 25 and 0x01f))
    append('_')
    append(showBits(this@binstr ushr 20 and 0x01f))
    append('_')
    append(showBits(this@binstr ushr 15 and 0x01f))
    append('_')
    append(showBits(this@binstr ushr 10 and 0x01f))
    append('_')
    append(showBits(this@binstr ushr 5 and 0x01f))
    append('_')
    append(showBits(this@binstr and 0x01f))
  }.toString()

private fun showBits(x: Int, l: Int = 5): String =
  (0 until l).fold("") { acc, b ->
    (if (x ushr b and 1 == 1) "1" else "0") + acc
  }