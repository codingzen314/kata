package tech.codingzen.kata.hamt

import org.junit.Test
import tech.codingzen.kata.hamt.TKey.Companion.tkey
import kotlin.test.assertEquals
import kotlin.test.assertNull

class HashCollisionNodeDelTest {
  @Test
  fun `del size 1 found`() = NodeTestSyntax {
    val entry = hashn("a", "b").del("a", 0)
    assertNull(entry)
  }

  @Test
  fun `del not found`() = NodeTestSyntax {
    val root = hashn("a", "b")
    val entry = root.del("b", 0)
    assertEquals(root, entry)
  }

  @Test
  fun `del start`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val kB = tkey("b", 2)
    val vB = "vb"

    val node = hashn(kA, vA, kB, vB).del(kA, 0)
    assertEquals(hashn(kB, vB), node)
  }

  @Test
  fun `del end`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val kB = tkey("b", 2)
    val vB = "vb"

    val node = hashn(kA, vA, kB, vB).del(kB, 0)
    assertEquals(hashn(kA, vA), node)
  }

  @Test
  fun `del middle`() = NodeTestSyntax {
    val kA = tkey("a", 2)
    val vA = "va"
    val kB = tkey("b", 2)
    val vB = "vb"
    val kC = tkey("c", 2)
    val vC = "vc"

    val node = hashn(kA, vA, kB, vB, kC, vC).del(kB, 0)
    assertEquals(hashn(kA, vA, kC, vC), node)
  }

}