package tech.codingzen.kata.either

import org.junit.Test
import kotlin.test.assertEquals

class MultipleBindTest {
  val good: Either<String, Int> = Either.right(10)
  val fail: Either<String, Int> = Either.left("fail")
  val corr: Either<String, String> = Either.right("correct")

  @Test
  fun tuple2() {
    assertEquals(corr, bindAll(good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail) { "correct" })
  }

  @Test
  fun tuple3() {
    assertEquals<Any>(corr, bindAll(good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail) { "correct" })
  }

  @Test
  fun tuple4() {
    assertEquals<Any>(corr, bindAll(good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, fail) { "correct" })
  }

  @Test
  fun tuple5() {
    assertEquals<Any>(corr, bindAll(good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, fail) { "correct" })
  }

  @Test
  fun tuple6() {
    assertEquals<Any>(corr, bindAll(good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, fail) { "correct" })
  }

  @Test
  fun tuple7() {
    assertEquals<Any>(corr, bindAll(good, good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, fail, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, good, fail) { "correct" })
  }

  @Test
  fun tuple8() {
    assertEquals<Any>(corr, bindAll(good, good, good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(fail, good, good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, fail, good, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, fail, good, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, fail, good, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, fail, good, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, fail, good, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, good, fail, good) { "correct" })
    assertEquals<Any>(fail, bindAll(good, good, good, good, good, good, good, fail) { "correct" })
  }

}