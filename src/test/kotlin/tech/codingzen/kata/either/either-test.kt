package tech.codingzen.kata.either

import org.junit.Test
import tech.codingzen.kata.either.Either.Companion.left
import tech.codingzen.kata.either.Either.Companion.right
import java.util.*
import kotlin.test.*

fun assertIsLeft(actual: Either<*, *>): Unit {
  asserter.assertTrue("expected to be Left", actual.isLeft())
}

fun assertIsNotLeft(actual: Either<*, *>): Unit {
  asserter.assertTrue("expected to not be Left", !actual.isLeft())
}

fun assertIsRight(actual: Either<*, *>): Unit {
  asserter.assertTrue("expected to be Right", actual.isRight())
}

fun assertIsNotRight(actual: Either<*, *>): Unit {
  asserter.assertTrue("expected to not be Right", !actual.isRight())
}

fun <L> assertLeft(expected: L, actual: Either<L, *>): Unit {
  when (actual) {
    is Left -> asserter.assertEquals("expected a left to be equal to $expected", expected, actual.left)
    is Right -> asserter.fail("expected a left to be equal to $expected")
  }
}

fun <R> assertRight(expected: R, actual: Either<*, R>): Unit {
  when (actual) {
    is Left -> asserter.fail("expected a left to be equal to $expected")
    is Right -> asserter.assertEquals("expected a left to be equal to $expected", expected, actual.right)
  }
}

class EitherTests {
  @Test
  fun fold_isLeft() {
    assertEquals(101, left("foo").fold({ 101 }, { 9 }))
  }

  @Test
  fun fold_isRight() {
    assertEquals(9, right("foo").fold({ 101 }, { 9 }))
  }

  @Test
  fun fold_verticalSyntax_isLeft() {
    val r = left("foo")
      .foldL { 101 }
      .foldR { 9 }
    assertEquals(101, r)
  }

  @Test
  fun fold_verticalSyntax_isRight() {
    val r = right("foo")
      .foldL { 101 }
      .foldR { 9 }
    assertEquals(9, r)
  }

  @Test
  fun isLeft_isLeft() {
    assertIsLeft(left(10))
  }

  @Test
  fun isLeft_isRight() {
    assertIsNotLeft(right(10))
  }

  @Test
  fun isRight_isLeft() {
    assertIsNotRight(left(10))
  }

  @Test
  fun isRight_isRight() {
    assertIsRight(right(10))
  }

  @Test
  fun bind_isLeft() {
    assertIsLeft(left(100).bind { right(9) })

    val e: Either<Int, String> = left(100)
    val e1 = e.bind { left(90) }
    assertIsLeft(e1)
  }

  @Test
  fun bind_isRight() {
    val e: Either<String, Int> = right(100)
    assertIsLeft(e.bind { left("") })
    assertRight(85, right(100).bind { right(it - 15) })
  }

  @Test
  fun map_isLeft() {
    assertIsLeft(left(100).map { "" })
  }

  @Test
  fun map_isRight() {
    assertRight(3, right("foo").map(String::length))
  }

  @Test
  fun filter_isLeft() {
    val e: Either<String, Int> = left("foo")
    val expected: Either<String, Int> = left("crap")
    assertIsLeft(e
      .filter {
        test = { it == 100 }
        handler = { expected }
      })
  }

  @Test
  fun filter_failed_isRight() {
    val e: Either<String, Int> = right(9)
    assertLeft("crap", e
      .filter {
        test = { it == 100 }
        handler = { left("crap") }
      })
  }

  @Test
  fun filter_passed_isRight() {
    val e: Either<String, Int> = right(9)
    assertRight(9,
      e.filter {
        test = { it == 9 }
        handler = { left("crap") }
      })
  }

  @Test
  fun onRight_isLeft() {
    var store: Int? = null
    val e = left(100)
    assertEquals(e, e.peekRight { store = 3 })
    assertNull(store)
  }

  @Test
  fun onRight_iRight() {
    var store: Int? = null
    val e = right(100)
    assertEquals(e, e.peekRight { store = it })
    assertEquals(100, store)
  }

  @Test
  fun onLeft_isLeft() {
    var store: Int? = null
    val e = left(100)
    assertEquals(e, e.peekLeft { store = it })
    assertEquals(100, store)
  }

  @Test
  fun onLeft_iRight() {
    var store: Int? = null
    val e = right(100)
    assertEquals(e, e.peekLeft { store = 3 })
    assertNull(store)
  }

  @Test
  fun right_isLeft() {
    assertFailsWith(NoSuchElementException::class) { left(100).right }
  }

  @Test
  fun right_isRight() {
    assertEquals("foo", right("foo").right)
  }

  @Test
  fun left_isLeft() {
    assertEquals("foo", left("foo").left)
  }

  @Test
  fun left_isRight() {
    assertFailsWith(NoSuchElementException::class) { left(100).right }
  }

  @Test
  fun orElse_isLeft() {
    assertEquals("foo", left(123).orElse("foo"))
  }

  @Test
  fun orElse_isRight() {
    assertEquals("foo", right("foo").orElse("bar"))
  }

  @Test
  fun orElseMap_isLeft() {
    assertEquals("bar", left(123).orElseMap { "bar" })
  }

  @Test
  fun orElseMap_isRight() {
    assertEquals("foo", right("foo").orElseMap { "bar" })
  }

  @Test
  fun orElseGet_isLeft() {
    assertEquals("bar", left(123).orElseGet { "bar" })
  }

  @Test
  fun orElseGet_isRight() {
    assertEquals("foo", right("foo").orElseGet { "bar" })
  }

  @Test
  fun orThrow_isLeft() {
    assertFailsWith(IllegalStateException::class) { left(100).orThrow { IllegalStateException("should be thrown") } }
  }

  @Test
  fun orThrow_isRight() {
    assertEquals(100, right(100).orThrow { IllegalStateException("should be thrown") })
  }

  @Test
  fun bindLeft_isRight() {
    assertIsRight(right(100).bindLeft { right(9) })
  }

  @Test
  fun bindLeft_isLeft() {
    val e: Either<Int, String> = left(100)
    assertIsRight(e.bindLeft { right("") })
    assertLeft(85, left(100).bindLeft { left(it - 15) })
  }

  @Test
  fun mapLeft_isRight() {
    assertIsRight(right(100).mapLeft { "" })
  }

  @Test
  fun mapLeft_isLeft() {
    assertLeft(3, left("foo").mapLeft(String::length))
  }

  @Test
  fun filterLeft_isRight() {
    val e: Either<Int, String> = right("foo")
    val expected: Either<Int, String> = left(11)
    assertIsRight(e
      .filterLeft {
        test = { it == 100 }
        handler = { expected }
      })
  }

  @Test
  fun filterLeft_failed_isLeft() {
    val e: Either<Int, String> = left(9)
    val expected: Either<Int, String> = left(10)
    assertEquals(expected,
      e.filterLeft {
        test = { it == 100 }
        handler = { expected }
      })
  }

  @Test
  fun filterLeft_passed_isLeft() {
    val e: Either<Int, String> = left(100)
    assertEquals(e,
      e.filterLeft {
        test = { it == 100 }
        handler = { right("not this one") }
      })
  }

  @Test
  fun join_viaRight() {
    val e0: Either<String, Either<String, Int>> = left("foo")
    assertLeft("foo", e0.join())

    val e1: Either<String, Either<String, Int>> = right(left("foo"))
    assertLeft("foo", e1.join())
  }

  @Test
  fun join_viaLeft() {
    val e0: Either<Either<String, Int>, String> = right("foo")
    assertRight("foo", e0.join())

    val e1: Either<Either<String, Int>, String> = left(left("foo"))
    assertLeft("foo", e1.join())
  }

  @Test
  fun bindTry_isLeft() {
    val e: Either<Char, String> = left('f')
    val e0 = e
      .bindTry { if (it == "foo") left('r') else right(34) }
      .handler { _, _ -> left('3') }
    assertEquals<Any>(e, e0)
  }

  @Test
  fun bindTry_isRight_throws() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .bindTry { right(UUID.fromString("lkasjf")) }
      .handler { _, _ -> left('3') }
    assertLeft('3', e0)
  }

  @Test
  fun bindTry_isRight() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .bindTry { right(Integer.parseInt("34")) }
      .handler { _, _ -> left('3') }
    assertRight(34, e0)
  }

  @Test
  fun mapTry_isLeft() {
    val e: Either<Char, String> = left('f')
    val e0 = e
      .mapTry { Integer.parseInt("2") }
      .handler { _, _ -> right(50) }
    assertEquals<Any>(e, e0)
  }

  @Test
  fun mapTry_isRight_throwsException() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .mapTry { Integer.parseInt("fr") }
      .handler { _, _ -> right(50) }
    assertRight(50, e0)
  }

  @Test
  fun mapTry_isRight() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .mapTry { Integer.parseInt("56") }
      .handler { _, _ -> left('k') }
    assertRight(56, e0)
  }

  @Test
  fun catching_throwException() {
    val e = Either.catching { Integer.parseInt("afj") }
    assertIsLeft(e)
    assertIs<NumberFormatException>(e.left)
  }

  @Test
  fun catching_value() {
    val e = Either.catching { Integer.parseInt("65") }
    assertRight(65, e)
  }

  @Test
  fun catchingFn_throwException() {
    val f = Either.catchingFn { s: String -> Integer.parseInt(s) }
    val e = f("foj")
    assertIsLeft(e)
    assertIs<NumberFormatException>(e.left)
  }

  @Test
  fun catchingFn_value() {
    val f = Either.catchingFn { s: String -> Integer.parseInt(s) }
    val e = f("65")
    assertRight(65, e)
  }

  @Test
  fun sequence_empty() {
    val e = listOf<Either<String, Int>>().sequence()
    assertIsRight(e)
    assertContentEquals(listOf(), e.right)
  }

  @Test
  fun sequence_single_success() {
    val e = listOf(Either.right("foo")).sequence()
    assertIsRight(e)
    assertContentEquals(listOf("foo"), e.right)
  }

  @Test
  fun sequence_single_error() {
    val e = listOf(Either.left("oops")).sequence()
    assertIsLeft(e)
    assertLeft("oops", e)
  }


  @Test
  fun sequence_list_success() {
    val e = listOf(
      Either.right("foo"),
      Either.right("bar"),
      Either.right("baz")
    )
      .sequence()
    assertIsRight(e)
    assertContentEquals(listOf("foo", "bar", "baz"), e.right)
  }

  @Test
  fun sequence_list_last_err() {
    val e = listOf(
      Either.right("foo"),
      Either.right("bar"),
      Either.left("oops")
    )
      .sequence()
    assertIsLeft(e)
    assertLeft("oops", e)
  }

  @Test
  fun get() {
    assertEquals(12, (Either.left(12) as Either<Int, Int>).get())
    assertEquals(12, (Either.right(12) as Either<Int, Int>).get())
  }

  @Test
  fun onLeft() {
    val f = fn@{ e: Either<String, Int> ->
      val x: Int = e.onLeft { return@fn it }
      right(x + 1)
    }

    val l = left("yep")
    assertEquals(l, f(l))
    assertRight(3, f(right(2)))
  }

  @Test
  fun onRight() {
    val f = fn@{ e: Either<String, Int> ->
      val x: String = e.onRight { return@fn it }
      left(x.toUpperCase())
    }

    val r = right(1)
    assertEquals(r, f(r))
    assertLeft("ABC", f(left("abc")))
  }
}